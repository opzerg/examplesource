﻿using OrderProgram.Common;
using OrderProgram.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static OrderProgram.Model.HospitalOrder;

namespace OrderProgram.MyControl
{
    public class MemoryControl
    {

        private static Dictionary<string, Hospital> hospitals 
            = new Dictionary<string, Hospital>();

        private static Dictionary<string, Partner> partners 
            = new Dictionary<string, Partner>();

        private static List<MyWork> myWorks = new List<MyWork>();

        private static Dictionary<string, Category> LargeCategories = 
            new Dictionary<string, Category>();
        private static Dictionary<string, Category> SmallCategories
            = new Dictionary<string, Category>();

        private static List<HospitalOrder> HospitalOrders = new List<HospitalOrder>();

        public virtual bool Add(CodeModel model)
        {
            if (model is Hospital)
            {
                hospitals[model.CODE] = model as Hospital;
            }
            else if (model is Partner)
            {
                partners[model.CODE] = model as Partner;
            }
            else if(model is MyWork)
            {
                myWorks.Add(model as MyWork);
            }
            else if(model is Category)
            {
                Category category = model as Category;
                if (category.KIND == MyCommon.CATEGORY_KIND.대분류)
                    LargeCategories[category.CODE] = category;
                else
                    SmallCategories[category.CODE] = category;
            }
            else if (model is HospitalOrder)
            {
                HospitalOrder hospitalOrder = model as HospitalOrder;
                HospitalOrders.Add(hospitalOrder);
            }

            return true;
        }

        public virtual void UpdateUsing(CodeModel codeModel)
        {
            if (codeModel is Hospital)
            {
                Hospital hospital = codeModel as Hospital;
                hospitals[hospital.CODE].FLAG = hospital.FLAG;
            }
            else if (codeModel is Partner)
            {
                Partner partner = codeModel as Partner;
                partners[partner.CODE].FLAG = partner.FLAG;
                
            }else if (codeModel is MyWork)
            {
                foreach (MyWork myWork in myWorks)
                {
                    if (myWork.CODE == codeModel.CODE &&
                        myWork.RequestDate == codeModel.RequestDate)
                    {
                        myWork.FLAG = codeModel.FLAG;
                        break;
                    }
                }
            }
            
        }

        public static List<Hospital> GetHospitals()
        {
            return new List<Hospital>(hospitals.Values);
        }

        internal static void UpdateOrder(Order nowOrder)
        {
            foreach(HospitalOrder hospitalOrder in HospitalOrders)
            {
                if(hospitalOrder.Sequence == nowOrder.HospitalSequnce)
                {
                    foreach(Order order in hospitalOrder.Orders)
                    {
                        if(order.Sequence == nowOrder.Sequence)
                        {
                            order.Change(nowOrder);
                            return;
                        }
                    }
                }
            }
        }

        internal void SetCategories(Dictionary<string, Category> categories, MyCommon.CATEGORY_KIND kind)
        {
            if(kind == MyCommon.CATEGORY_KIND.대분류)
            {
                LargeCategories.Clear();
                LargeCategories = categories;
            }
            else
            {
                SmallCategories.Clear();
                SmallCategories = categories;
            }

        }

        internal void SetHospitals(Dictionary<string, Hospital> hospitals)
        {
            MemoryControl.hospitals.Clear();
            MemoryControl.hospitals = hospitals;
        }

        protected void SetWorks(List<MyWork> myWorks)
        {
            MemoryControl.myWorks.Clear();
            MemoryControl.myWorks.AddRange(myWorks);
        }

        internal static bool CompareCode(CodeModel codeModel)
        {
            if (codeModel is Hospital)
            {
                return hospitals.ContainsKey(codeModel.CODE);
            }
            else if (codeModel is Partner)
            {
                return partners.ContainsKey(codeModel.CODE);
            }
            else
                return false;
        }

        internal virtual void Delete(CodeModel codeModel)
        {
            if (codeModel is Hospital)
            {
                Hospital hospital = codeModel as Hospital;
                hospitals.Remove(hospital.CODE);
            }
            else if (codeModel is Partner)
            {
                Partner partner = codeModel as Partner;
                partners.Remove(partner.CODE);
            }
            else if (codeModel is MyWork)
            {
                MyWork myWork = codeModel as MyWork;
                myWorks.Remove(myWork);
            }
            else if(codeModel is Category)
            {
                Category category = codeModel as Category;
                if (category.KIND == MyCommon.CATEGORY_KIND.대분류)
                    LargeCategories.Remove(category.CODE);
                else
                    SmallCategories.Remove(category.CODE);
            }else if(codeModel is HospitalOrder)
            {
                HospitalOrder hospitalOrder = codeModel as HospitalOrder;
                HospitalOrders.Remove(hospitalOrder);
            }
        }
        internal static List<Category> GetCategories(MyCommon.CATEGORY_KIND kind)
        {
            if (kind == MyCommon.CATEGORY_KIND.대분류)
                return  new List<Category>(LargeCategories.Values);
            else
                return new List<Category>(SmallCategories.Values);
        }

        internal static List<Partner> GetPartnersToList()
        {
            return new List<Partner>(partners.Values);
        }

        internal static List<MyWork> GetMyWorks()
        {
            return myWorks;
        }

        public virtual void Update(CodeModel model)
        {
            if(model is Hospital)
            {
                hospitals[model.CODE] = model as Hospital;
            }
            else if(model is Partner)
            {
                partners[model.CODE] = model as Partner;
            }
        }

        internal void SetPartners(Dictionary<string, Partner> partners)
        {
            MemoryControl.partners.Clear();
            MemoryControl.partners = partners;
        }

        internal static object GetModelFromCode(CodeModel codeModel)
        {
            if (codeModel is Hospital)
            {
                return hospitals[codeModel.CODE];
            }
            else if (codeModel is Partner)
            {
                return partners[codeModel.CODE];
            }
            else if(codeModel is Category)
            {
                Category category = codeModel as Category;
                if (category.KIND == MyCommon.CATEGORY_KIND.대분류)
                    return LargeCategories[category.CODE];
                else
                    return SmallCategories[category.CODE];
            }
            else
                return new CodeModel();
        }

        internal virtual void UpdateStatus(CodeModel codeModel)
        {
            if (codeModel is MyWork)
            {
                foreach (MyWork myWork in myWorks)
                {
                    if (myWork.CODE == codeModel.CODE)
                    {
                        myWork.Status = codeModel.Status;
                        break;
                    }
                }
            }
        }

        internal void SetHospitalOrders(List<HospitalOrder> hospitalOrders)
        {
            MemoryControl.HospitalOrders.Clear();
            MemoryControl.HospitalOrders.AddRange(hospitalOrders);
        }

        public static List<HospitalOrder> GetHospitalOrders()
        {
            return MemoryControl.HospitalOrders;
        }

        internal static void DeleteOrder(Order nowOrder)
        {
            foreach(HospitalOrder hospitalOrder in HospitalOrders)
            {
                if(hospitalOrder.Sequence == nowOrder.HospitalSequnce)
                {
                    hospitalOrder.Orders.Remove(nowOrder);
                    return;
                }
            }
        }
    }
}
