﻿using OrderProgram.Common;
using OrderProgram.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;
using static OrderProgram.Model.HospitalOrder;

namespace OrderProgram.MyControl
{
    public class DBControl : MemoryControl
    {
        private readonly string connectionstring = @"Server=YONGGUK-SURFACE\SQLEXPRESS;database=EHSolution;Integrated Security=SSPI;";
        private static char STX = Convert.ToChar(0x02);
        public int LargeCategoryCode
        {
            get;
            set;
        }
        public int SmallCategoryCode
        {
            get; set;
        }

        #region Singleton
        private static DBControl instance = null;
        private DBControl()
        {
        }
        public static DBControl Instance
        {
            get
            {
                if (instance == null)
                    instance = new DBControl();
                return instance;
            }
        }
        #endregion

        public void Init()
        {
            SetHospitals();
            SetPartners();
            SetCategories();
        }

        internal void SetCategories()
        {
            foreach(MyCommon.CATEGORY_KIND kind in (MyCommon.CATEGORY_KIND[])Enum.GetValues(typeof(MyCommon.CATEGORY_KIND)))
            {
                Category category = new Category(kind);
                string tableName = GetTableName(category);
                Dictionary<string, Category> categories = 
                    new Dictionary<string, Category>();

                SqlCommand sqlCommand = Connect();
                sqlCommand.CommandText = $"select * from {tableName}";
                sqlCommand.Connection.Open();

                using (SqlDataReader dr = sqlCommand.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        string code = ((int)dr["code"]).ToString();
                        string name = dr["name"] as string;
                        Category cg = new Category(code, name, kind);
                        categories[cg.CODE] = cg;
                    }
                }
                sqlCommand.Connection.Close();

                if (categories.Count == 0)
                    this.LargeCategoryCode = 1;
                else
                    SetCategoryCode(tableName);

                base.SetCategories(categories, kind);
            }
        }

        private void SetCategoryCode(string tableName)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"select IDENT_CURRENT('{tableName}') AS code";
            sqlCommand.Connection.Open();
            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    if (tableName.Equals("LARGE_CATEGORY_TB"))
                        this.LargeCategoryCode = Convert.ToInt32(dr[0]) + 1;
                    else
                        this.SmallCategoryCode = Convert.ToInt32(dr[0]) + 1;
                }
            }
            sqlCommand.Connection.Close();
        }

        internal int GetHOSequence(HospitalOrder hospitalOrder)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"select sequence from ORDER_HOSPITAL_TB " +
                $"where request_date = '{hospitalOrder.RequestDate}' and " +
                $"hospital_code = {hospitalOrder.Hospital.CODE} and " +
                $"partner_code = {hospitalOrder.Partner.CODE}";

            sqlCommand.Connection.Open();

            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    return (int)dr[0];
                }
            }
            sqlCommand.Connection.Close();
            return 0;
        }

        internal void DeleteTreeCategory(string large_code, string small_code)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"delete from CATEGORY_TREE_TB where large_code = @large_code and small_code = @small_code";
            sqlCommand.Parameters.AddWithValue("@large_code", large_code);
            sqlCommand.Parameters.AddWithValue("@small_code", small_code);

            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        internal List<Category> GetCategoryTree(string large_code)
        {
            List<Category> categories = new List<Category>();
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"select CATEGORY_TREE_TB.large_code, CATEGORY_TREE_TB.small_code, SMALL_CATEGORY_TB.name " +
                $"from CATEGORY_TREE_TB inner join SMALL_CATEGORY_TB on " +
                $"CATEGORY_TREE_TB.small_code = SMALL_CATEGORY_TB.code " +
                $"Where large_code = {large_code}";
            sqlCommand.Connection.Open();

            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    string code = ((int)dr["small_code"]).ToString();
                    string name = dr["name"] as string;
                    Category cg = new Category(code, name, MyCommon.CATEGORY_KIND.소분류);
                    categories.Add(cg);
                }
            }
            sqlCommand.Connection.Close();
            return categories;
        }

        internal void SetWorks(string pre_requestDate, string from_requestDate)
        {
            List<MyWork> myWorks = new List<MyWork>();
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"select * from WORK_TB where (request_date between '{pre_requestDate}' and " +
                $"'{from_requestDate}')"; //{StatusQuery(statusList)}";

            sqlCommand.Connection.Open();
            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    DateTime request_date = (DateTime)dr["request_date"];
                    string partner_code = dr["partner_code"] as string;
                    Partner partner = MemoryControl.GetModelFromCode(new Partner(partner_code)) as Partner;
                    string works = dr["works"] as string;
                    List<string> workList = MakeWorksFormatFromString(works);
                    int status = (short)dr["status"];
                    int flag = (short)dr["flag"];
                    MyCommon.FLAG flags = (MyCommon.FLAG)flag;
                    MyCommon.STATUS stat = (MyCommon.STATUS)status;
                    MyWork myWork = new MyWork(request_date.ToString("yyyy/MM/dd HH:mm:ss"), partner, workList, stat, flags);
                    myWorks.Add(myWork);
                }
            }
            sqlCommand.Connection.Close();
            base.SetWorks(myWorks);
        }

        internal new void UpdateOrder(Order order)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.Connection.Open();
            sqlCommand.CommandText = $"update ORDER_TB " +
                $"set kind = @kind, large = @large, small = @small, box = @box, price = @price, surtax = @surtax " +
            $"where sequence = @sequence and hospital_sequence = @hospital_sequence";
            sqlCommand.Parameters.AddWithValue("@kind", (short)order.OrderKind);
            sqlCommand.Parameters.AddWithValue("@large", order.LargeCategory.CODE);
            sqlCommand.Parameters.AddWithValue("@small", order.SmallCategory.CODE);
            sqlCommand.Parameters.AddWithValue("@box", order.Box);
            sqlCommand.Parameters.AddWithValue("@price", order.Price);
            sqlCommand.Parameters.AddWithValue("@surtax", order.Surtax);
            sqlCommand.Parameters.AddWithValue("@sequence", order.Sequence);
            sqlCommand.Parameters.AddWithValue("@hospital_sequence", order.HospitalSequnce);
                
            sqlCommand.ExecuteNonQuery();

            sqlCommand.Connection.Close();

        }

        internal void AddOrder(HospitalOrder hospitalOrder)
        {
            if (hospitalOrder != null)
            {
                foreach (Order order in hospitalOrder.Orders)
                {
                    OrderAdd(order);
                }
            }
        }

        private string StatusQuery(MyCommon.STATUS[] statusList)
        {
            string query = "or status=";

            for (int idx = 0; idx < statusList.Count() - 1; idx++)
            {
                MyCommon.STATUS status = statusList[idx];
                if (idx is 0)
                    query = $"{query}{(short)status}";
                else
                    query = $"{query} or status={(short)status}";
            }

            return query;
        }

        private List<string> MakeWorksFormatFromString(string works)
        {
            string[] strArray = works.Split(STX);
            return strArray.ToList();
        }

        private SqlCommand Connect()
        {
            SqlConnection sqlConnection = new SqlConnection(connectionstring);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            return sqlCommand;
        }

        internal List<HospitalOrder> GetAllHospitalOrders(string hospitalCode)
        {
            List<HospitalOrder> hospitalOrders = new List<HospitalOrder>();
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"select * from ORDER_HOSPITAL_TB where hospital_code = {hospitalCode}";
            sqlCommand.Connection.Open();
            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    int sequence = (int)dr["sequence"];
                    DateTime requestDate = (DateTime)dr["request_date"];
                    string partner_code = dr["partner_code"] as string;
                    string hospital_code = dr["hospital_code"] as string;
                    MyCommon.STATUS status = (MyCommon.STATUS)((short)dr["status"]);

                    Partner partner = MemoryControl.GetModelFromCode(new Partner(partner_code)) as Partner;
                    Hospital hospital = MemoryControl.GetModelFromCode(new Hospital(hospital_code)) as Hospital;

                    HospitalOrder hospitalOrder = new HospitalOrder(sequence,
                        requestDate.ToString("yyyy/MM/dd HH:mm:ss"), hospital, partner, status);


                    hospitalOrders.Add(hospitalOrder);
                }
            }
            sqlCommand.Connection.Close();

            foreach (HospitalOrder hospitalOrder in hospitalOrders)
            {
                GetOrdersFromSequence(hospitalOrder.Sequence, hospitalOrder);
            }
            return hospitalOrders;
        }

        internal void AddTreeCategory(Category large, Category small)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"insert into CATEGORY_TREE_TB values(@large_code, @small_code)";
            sqlCommand.Parameters.AddWithValue("@large_code", large.CODE);
            sqlCommand.Parameters.AddWithValue("@small_code", small.CODE);

            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        public override bool Add(CodeModel model)
        {
            if(model is Hospital)
            {
                HospitalAdd(model as Hospital);
            }
            else if(model is Partner)
            {
                PartnerAdd(model as Partner);
            }
            else if(model is MyWork)
            {
                WorkAdd(model as MyWork);
            }
            else if(model is Category)
            {
                CategoryAdd(model as Category);
            } else if(model is HospitalOrder)
            {
                HospitalOrderAdd(model as HospitalOrder);
            }

            return base.Add(model);
        }

        private void OrderAdd(Order order)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"insert into ORDER_TB " +
                $"values(@sequence, @hospital_sequence, @kind, @large, @small, @box, @price, @surtax)";
            sqlCommand.Parameters.AddWithValue("@sequence", order.Sequence);
            sqlCommand.Parameters.AddWithValue("@hospital_sequence", order.HospitalSequnce);
            sqlCommand.Parameters.AddWithValue("@kind", (short)order.OrderKind);
            sqlCommand.Parameters.AddWithValue("@large", order.LargeCategory.CODE);
            sqlCommand.Parameters.AddWithValue("@small", order.SmallCategory.CODE);
            sqlCommand.Parameters.AddWithValue("@box", order.Box);
            sqlCommand.Parameters.AddWithValue("@price", order.Price);
            sqlCommand.Parameters.AddWithValue("@surtax", order.Surtax);

            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        private void HospitalOrderAdd(HospitalOrder hospitalOrder)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"insert into ORDER_HOSPITAL_TB (request_date, hospital_code, partner_code, status) " +
                $"values(@request_date, @hospital_code, @partner_code, @status)";
            sqlCommand.Parameters.AddWithValue("@request_date", hospitalOrder.RequestDate);
            sqlCommand.Parameters.AddWithValue("@hospital_code", hospitalOrder.Hospital.CODE);
            sqlCommand.Parameters.AddWithValue("@partner_code", hospitalOrder.Partner.CODE);
            sqlCommand.Parameters.AddWithValue("@status", (short)hospitalOrder.Status);

            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();

            
        }

        internal void SetHospitalOrders(string pre_date, string from_date)
        {
            List<HospitalOrder> hospitalOrders = new List<HospitalOrder>();
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"select * from ORDER_HOSPITAL_TB where (request_date between '{pre_date}' and " +
                $"'{from_date}')"; 

            sqlCommand.Connection.Open();
            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    int sequence = (int)dr["sequence"];
                    DateTime requestDate = (DateTime)dr["request_date"];
                    string partner_code = dr["partner_code"] as string;
                    string hospital_code = dr["hospital_code"] as string;
                    MyCommon.STATUS status = (MyCommon.STATUS)((short)dr["status"]);

                    Partner partner = MemoryControl.GetModelFromCode(new Partner(partner_code)) as Partner;
                    Hospital hospital = MemoryControl.GetModelFromCode(new Hospital(hospital_code)) as Hospital;

                    HospitalOrder hospitalOrder = new HospitalOrder(sequence, 
                        requestDate.ToString("yyyy/MM/dd HH:mm:ss"), hospital, partner, status);
                    

                    hospitalOrders.Add(hospitalOrder);
                }
            }
            sqlCommand.Connection.Close();

            foreach(HospitalOrder hospitalOrder in hospitalOrders)
            {
                GetOrdersFromSequence(hospitalOrder.Sequence, hospitalOrder);
            }
            base.SetHospitalOrders(hospitalOrders);
        }

        internal void GetOrdersFromSequence(int hospitalSequence, HospitalOrder hospitalOrder)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"select * from ORDER_TB where hospital_sequence = {hospitalSequence}";

            sqlCommand.Connection.Open();
            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    
                    int sequence = (int)dr["sequence"];
                    MyCommon.ORDER_KIND kind = (MyCommon.ORDER_KIND)((short)dr["kind"]);
                    int large_code = (int)dr["large"];
                    int small_code = (int)dr["small"];
                    int box = (int)dr["box"];
                    long price = (long)dr["price"];
                    long surtax = (long)dr["surtax"];
                    
                    Category large = MemoryControl.GetModelFromCode(new Category(large_code.ToString(), 
                        MyCommon.CATEGORY_KIND.대분류)) as Category;
                    Category small = MemoryControl.GetModelFromCode(new Category(small_code.ToString(),
                        MyCommon.CATEGORY_KIND.소분류)) as Category; 

                    Order order = new Order(sequence, hospitalSequence, kind, large, small, box, price, surtax);
                    hospitalOrder.Orders.Add(order);
                }
            }
            sqlCommand.Connection.Close();

        }
        private void CategoryAdd(Category category)
        {
            SqlCommand sqlCommand = Connect();
            string tableName = GetTableName(category);
            sqlCommand.CommandText = $"insert into {tableName}(name) " +
                $"values(@name)";
            sqlCommand.Parameters.AddWithValue("@name", category.NAME);
            
            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        private void WorkAdd(MyWork myWork)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"insert into WORK_TB(request_date, partner_code, works, status, flag) " +
                $"values(@request_date, @partner_code, @works, @status, @flag)";
            sqlCommand.Parameters.AddWithValue("@request_date", myWork.RequestDate);
            sqlCommand.Parameters.AddWithValue("@partner_code", myWork.Partner.CODE);
            string works = MakeWorksFormatFromList(myWork.Works);
            sqlCommand.Parameters.AddWithValue("@works", works);
            sqlCommand.Parameters.AddWithValue("@status", (short)MyCommon.STATUS.진행);
            sqlCommand.Parameters.AddWithValue("@flag", (short)MyCommon.FLAG.USING);

            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        private string MakeWorksFormatFromList(List<string> works)
        {
            string return_work = string.Empty;
            foreach(var work in works)
            {
                if (return_work == string.Empty)
                    return_work = work;
                else
                    return_work = $"{return_work}{STX}{work}";
            }
            return return_work;
        }

        private void PartnerAdd(Partner partner)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"insert into PARTNER_TB values(@code, @name, @phone_number, @memo, @flag)";
            sqlCommand.Parameters.AddWithValue("@code", partner.CODE);
            sqlCommand.Parameters.AddWithValue("@name", partner.NAME);
            sqlCommand.Parameters.AddWithValue("@phone_number", partner.PHONE);
            sqlCommand.Parameters.AddWithValue("@memo", partner.MEMO);
            sqlCommand.Parameters.AddWithValue("@flag", (short)MyCommon.FLAG.USING);

            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        public void SetHospitals()
        {
            Dictionary<string, Hospital> hospitals 
                = new Dictionary<string, Hospital>();
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = "select * from HOSPITAL_TB";
            sqlCommand.Connection.Open();
            using (SqlDataReader dr =sqlCommand.ExecuteReader())
            {
                while(dr.Read())
                {
                    string code = dr["code"] as string;
                    string name = dr["name"] as string;
                    string manager = dr["manager"] as string;
                    string phone = dr["phone_number"] as string;
                    string address = dr["address"] as string;
                    string memo = dr["memo"] as string;
                    int flag = (short)dr["flag"];
                    MyCommon.FLAG flags = (MyCommon.FLAG)flag;

                    Hospital hospital = new Hospital(code, name, manager, phone, address, memo, flags);
                    hospitals[code] = hospital;
                }
            }
            sqlCommand.Connection.Close();
            base.SetHospitals(hospitals);
        }

        public void SetPartners()
        {
            Dictionary<string, Partner> partners = 
                new Dictionary<string, Partner>();
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = "select * from PARTNER_TB";
            sqlCommand.Connection.Open();
            using (SqlDataReader dr = sqlCommand.ExecuteReader())
            {
                while (dr.Read())
                {
                    string code = dr["code"] as string;
                    string name = dr["name"] as string;
                    string phone = dr["phone_number"] as string;
                    string memo = dr["memo"] as string;
                    int flag = (short)dr["flag"];
                    MyCommon.FLAG flags = (MyCommon.FLAG)flag;

                    Partner partner = new Partner(code, name, phone, memo, flags);
                    partners[code] = partner;
                }
            }
            sqlCommand.Connection.Close();
            base.SetPartners(partners);
        }

        internal override void Delete(CodeModel codeModel)
        {
            string tableName = GetTableName(codeModel);
            
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"delete from {tableName} where code = @code";
            sqlCommand.Parameters.AddWithValue("@code", codeModel.CODE);

            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();

            if (codeModel is Category)
                DeleteCategory(codeModel as Category);

            base.Delete(codeModel);
        }

        private void DeleteCategory(Category category)
        {
            SqlCommand sqlCommand = Connect();
            string column = (category.KIND == MyCommon.CATEGORY_KIND.대분류) ? "large_code" : "small_code";
            sqlCommand.CommandText = $"delete from CATEGORY_TREE_TB where {column} = @code";
            sqlCommand.Parameters.AddWithValue("@code", category.CODE);

            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        internal void DeleteHospitalOrder(HospitalOrder hospitalOrder)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"delete from ORDER_HOSPITAL_TB where sequence = @sequence";
            sqlCommand.Parameters.AddWithValue("@sequence", hospitalOrder.Sequence);

            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();

            DeleteOrders(hospitalOrder);

            base.Delete(hospitalOrder);
        }

        public void DeleteOrders(HospitalOrder hospitalOrder)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"delete from ORDER_TB where hospital_sequence = @sequence";
            sqlCommand.Parameters.AddWithValue("@sequence", hospitalOrder.Sequence);

            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        public override void Update(CodeModel model)
        {
            if (model is Hospital)
                HospitalUpdate(model as Hospital);
            else if (model is Partner)
                PartnerUpdate(model as Partner);
            else if (model is MyWork)
                MyWorkUpdate(model as MyWork);

            base.Update(model);
        }

        private void MyWorkUpdate(MyWork myWork)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"update WORK_TB set works = @works, change_date = @change_date" +
                $"where request_date = @request_date and partner_code = @code";
            string works = MakeWorksFormatFromList(myWork.Works);
            sqlCommand.Parameters.AddWithValue("@works", works);
            sqlCommand.Parameters.AddWithValue("@change_date", $"{DateTime.Now:yyyy/MM/dd HHmmss}");
            sqlCommand.Parameters.AddWithValue("@request_date", myWork.RequestDate);
            sqlCommand.Parameters.AddWithValue("@code", myWork.CODE);


            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        private void PartnerUpdate(Partner partner)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"update PARTNER_TB set name = @name, phone_number = @phone_number, " +
                $"memo = @memo, flag = @flag where code = @code";
            sqlCommand.Parameters.AddWithValue("@name", partner.NAME);
            sqlCommand.Parameters.AddWithValue("@phone_number", partner.PHONE);
            sqlCommand.Parameters.AddWithValue("@memo", partner.MEMO);
            sqlCommand.Parameters.AddWithValue("@flag", (short)MyCommon.FLAG.USING);
            sqlCommand.Parameters.AddWithValue("@code", partner.CODE);


            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        internal override void UpdateStatus(CodeModel codeModel)
        {
            string tableName = GetTableName(codeModel);
            string codeName = GetCodeName(codeModel);
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"update {tableName} set status = @status " +
                $"where request_date = @request_date and {codeName} = @code";
            sqlCommand.Parameters.AddWithValue("@status", (short)codeModel.Status);
            sqlCommand.Parameters.AddWithValue("@code", codeModel.CODE);
            sqlCommand.Parameters.AddWithValue("@request_date", codeModel.RequestDate);

            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();

            base.UpdateStatus(codeModel);
        }

        private void HospitalUpdate(Hospital hospital)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"update HOSPITAL_TB set name = @name, manager = @manager, phone_number = @phone_number, " +
                $"address = @address, memo = @memo, flag = @flag where code = @code";
            sqlCommand.Parameters.AddWithValue("@name", hospital.NAME);
            sqlCommand.Parameters.AddWithValue("@manager", hospital.MANAGER);
            sqlCommand.Parameters.AddWithValue("@phone_number", hospital.PHONE);
            sqlCommand.Parameters.AddWithValue("@address", hospital.ADDRESS);
            sqlCommand.Parameters.AddWithValue("@memo", hospital.MEMO);
            sqlCommand.Parameters.AddWithValue("@flag", (short)MyCommon.FLAG.USING);
            sqlCommand.Parameters.AddWithValue("@code", hospital.CODE);


            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }

        public override void UpdateUsing(CodeModel codeModel)
        {
            string tableName = GetTableName(codeModel);
            codeModel.FLAG = (codeModel.FLAG == MyCommon.FLAG.USING) ? MyCommon.FLAG.NONUSING : MyCommon.FLAG.USING;
            SqlCommand sqlCommand = Connect();
            string query = string.Empty;
            if (codeModel.RequestDate != string.Empty)
            {
                string codeName = GetCodeName(codeModel);
                query = $"update {tableName} set flag = @flag where request_date = @request_date and {codeName} = @code";
                sqlCommand.Parameters.AddWithValue("@request_date", codeModel.RequestDate);
            }
            else
            {
                query = $"update {tableName} set flag = @flag where code = @code";
            }

            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@flag", (short)codeModel.FLAG);
            sqlCommand.Parameters.AddWithValue("@code", codeModel.CODE);

            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();

            base.UpdateUsing(codeModel);
        }

        private string GetCodeName(CodeModel codeModel)
        {
            if (codeModel is MyWork)
                return "partner_code";
            else if (codeModel is HospitalOrder)
                return "sequence";
            else
                return string.Empty;
        }

        private string GetTableName(CodeModel codeModel)
        {
            if (codeModel is Hospital)
                return "HOSPITAL_TB";
            else if (codeModel is Partner)
                return "PARTNER_TB";
            else if (codeModel is MyWork)
                return "WORK_TB";
            else if (codeModel is Category)
            {
                Category category = codeModel as Category;
                if (category.KIND == MyCommon.CATEGORY_KIND.대분류)
                    return "LARGE_CATEGORY_TB";
                else
                    return "SMALL_CATEGORY_TB";
            } else if(codeModel is HospitalOrder)
            {
                return "ORDER_HOSPITAL_TB";
            }
            else
                return string.Empty;
        }

        private void HospitalAdd(Hospital hospital)
        {
            SqlCommand sqlCommand = Connect();
            sqlCommand.CommandText = $"insert into HOSPITAL_TB values(@code, @name, @manager, @phone_number, @address, @memo, @flag)";
            sqlCommand.Parameters.AddWithValue("@code", hospital.CODE);
            sqlCommand.Parameters.AddWithValue("@name", hospital.NAME);
            sqlCommand.Parameters.AddWithValue("@manager", hospital.MANAGER);
            sqlCommand.Parameters.AddWithValue("@phone_number", hospital.PHONE);
            sqlCommand.Parameters.AddWithValue("@address", hospital.ADDRESS);
            sqlCommand.Parameters.AddWithValue("@memo", hospital.MEMO);
            sqlCommand.Parameters.AddWithValue("@flag", (short)MyCommon.FLAG.USING);

            sqlCommand.Connection.Open();
            sqlCommand.ExecuteNonQuery();
            sqlCommand.Connection.Close();
        }
    }
}
