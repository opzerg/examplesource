﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderProgram.Common
{
    public static class MyCommon
    {
        public enum ORDER_KIND
        {
            유상,
            무상,
            서비스,
            NONE
        }
        public enum STATUS
        {
            완료,
            진행,
            보류,
            취소,
            전체,
            NONE
        }
        public enum CATEGORY_KIND
        {
            대분류,
            소분류
        }
        public enum FLAG
        {
            NONUSING = 0,
            USING = 1,
            ALL
        }

        internal static Color GetBackColorFromStatus(STATUS status)
        {
            switch(status)
            {
                case STATUS.보류:
                    return Color.LightYellow;
                case STATUS.완료:
                    return Color.LightGray;
                case STATUS.취소:
                    return Color.Yellow;
                default:
                    return Color.White;
            }
        }

        internal static (string, Color) GetBackColorFromNonUsing() => ("삭제", Color.Red);

    }
}
