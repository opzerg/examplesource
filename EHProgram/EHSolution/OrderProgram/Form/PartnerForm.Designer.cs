﻿namespace OrderProgram
{
    partial class PartnerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.lv_partnerList = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.보기ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.전체ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.사용ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.미사용ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.사용ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.미사용ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.새로고침F5ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tb_partnerCode = new System.Windows.Forms.TextBox();
            this.tb_partnerName = new System.Windows.Forms.TextBox();
            this.tb_parterPhoneNumber = new System.Windows.Forms.TextBox();
            this.tb_partnerMemo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_add = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lv_partnerList);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btn_add);
            this.splitContainer1.Panel2.Controls.Add(this.tb_partnerCode);
            this.splitContainer1.Panel2.Controls.Add(this.tb_partnerName);
            this.splitContainer1.Panel2.Controls.Add(this.tb_parterPhoneNumber);
            this.splitContainer1.Panel2.Controls.Add(this.tb_partnerMemo);
            this.splitContainer1.Panel2.Controls.Add(this.label6);
            this.splitContainer1.Panel2.Controls.Add(this.label4);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Size = new System.Drawing.Size(617, 314);
            this.splitContainer1.SplitterDistance = 207;
            this.splitContainer1.SplitterWidth = 6;
            this.splitContainer1.TabIndex = 1;
            // 
            // lv_partnerList
            // 
            this.lv_partnerList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lv_partnerList.ContextMenuStrip = this.contextMenuStrip1;
            this.lv_partnerList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv_partnerList.FullRowSelect = true;
            this.lv_partnerList.Location = new System.Drawing.Point(0, 0);
            this.lv_partnerList.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lv_partnerList.MultiSelect = false;
            this.lv_partnerList.Name = "lv_partnerList";
            this.lv_partnerList.Size = new System.Drawing.Size(207, 314);
            this.lv_partnerList.TabIndex = 0;
            this.lv_partnerList.UseCompatibleStateImageBehavior = false;
            this.lv_partnerList.View = System.Windows.Forms.View.Details;
            this.lv_partnerList.SelectedIndexChanged += new System.EventHandler(this.lv_partnerList_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "코드";
            this.columnHeader1.Width = 59;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "이름";
            this.columnHeader2.Width = 143;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.보기ToolStripMenuItem,
            this.사용ToolStripMenuItem,
            this.미사용ToolStripMenuItem,
            this.새로고침F5ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(226, 148);
            // 
            // 보기ToolStripMenuItem
            // 
            this.보기ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.전체ToolStripMenuItem,
            this.사용ToolStripMenuItem1,
            this.미사용ToolStripMenuItem1});
            this.보기ToolStripMenuItem.Name = "보기ToolStripMenuItem";
            this.보기ToolStripMenuItem.Size = new System.Drawing.Size(225, 36);
            this.보기ToolStripMenuItem.Text = "보기";
            // 
            // 전체ToolStripMenuItem
            // 
            this.전체ToolStripMenuItem.Name = "전체ToolStripMenuItem";
            this.전체ToolStripMenuItem.Size = new System.Drawing.Size(186, 38);
            this.전체ToolStripMenuItem.Text = "전체";
            this.전체ToolStripMenuItem.Click += new System.EventHandler(this.전체ToolStripMenuItem_Click);
            // 
            // 사용ToolStripMenuItem1
            // 
            this.사용ToolStripMenuItem1.Name = "사용ToolStripMenuItem1";
            this.사용ToolStripMenuItem1.Size = new System.Drawing.Size(186, 38);
            this.사용ToolStripMenuItem1.Text = "사용";
            this.사용ToolStripMenuItem1.Click += new System.EventHandler(this.사용ToolStripMenuItem1_Click);
            // 
            // 미사용ToolStripMenuItem1
            // 
            this.미사용ToolStripMenuItem1.Name = "미사용ToolStripMenuItem1";
            this.미사용ToolStripMenuItem1.Size = new System.Drawing.Size(186, 38);
            this.미사용ToolStripMenuItem1.Text = "미사용";
            this.미사용ToolStripMenuItem1.Click += new System.EventHandler(this.미사용ToolStripMenuItem1_Click);
            // 
            // 사용ToolStripMenuItem
            // 
            this.사용ToolStripMenuItem.Name = "사용ToolStripMenuItem";
            this.사용ToolStripMenuItem.Size = new System.Drawing.Size(225, 36);
            this.사용ToolStripMenuItem.Text = "사용";
            this.사용ToolStripMenuItem.Click += new System.EventHandler(this.사용ToolStripMenuItem_Click);
            // 
            // 미사용ToolStripMenuItem
            // 
            this.미사용ToolStripMenuItem.Name = "미사용ToolStripMenuItem";
            this.미사용ToolStripMenuItem.Size = new System.Drawing.Size(225, 36);
            this.미사용ToolStripMenuItem.Text = "미사용";
            this.미사용ToolStripMenuItem.Click += new System.EventHandler(this.미사용ToolStripMenuItem_Click);
            // 
            // 새로고침F5ToolStripMenuItem
            // 
            this.새로고침F5ToolStripMenuItem.Name = "새로고침F5ToolStripMenuItem";
            this.새로고침F5ToolStripMenuItem.Size = new System.Drawing.Size(225, 36);
            this.새로고침F5ToolStripMenuItem.Text = "새로고침(F5)";
            this.새로고침F5ToolStripMenuItem.Click += new System.EventHandler(this.새로고침F5ToolStripMenuItem_Click);
            // 
            // tb_partnerCode
            // 
            this.tb_partnerCode.Location = new System.Drawing.Point(96, 21);
            this.tb_partnerCode.Name = "tb_partnerCode";
            this.tb_partnerCode.Size = new System.Drawing.Size(294, 51);
            this.tb_partnerCode.TabIndex = 2;
            this.tb_partnerCode.Leave += new System.EventHandler(this.tb_partnerCode_Leave);
            // 
            // tb_partnerName
            // 
            this.tb_partnerName.Location = new System.Drawing.Point(96, 62);
            this.tb_partnerName.Name = "tb_partnerName";
            this.tb_partnerName.Size = new System.Drawing.Size(294, 51);
            this.tb_partnerName.TabIndex = 3;
            // 
            // tb_parterPhoneNumber
            // 
            this.tb_parterPhoneNumber.Location = new System.Drawing.Point(96, 104);
            this.tb_parterPhoneNumber.Name = "tb_parterPhoneNumber";
            this.tb_parterPhoneNumber.Size = new System.Drawing.Size(294, 51);
            this.tb_parterPhoneNumber.TabIndex = 5;
            // 
            // tb_partnerMemo
            // 
            this.tb_partnerMemo.Location = new System.Drawing.Point(96, 149);
            this.tb_partnerMemo.Multiline = true;
            this.tb_partnerMemo.Name = "tb_partnerMemo";
            this.tb_partnerMemo.Size = new System.Drawing.Size(294, 121);
            this.tb_partnerMemo.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 152);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 38);
            this.label6.TabIndex = 6;
            this.label6.Text = "메모";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 107);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(169, 38);
            this.label4.TabIndex = 4;
            this.label4.Text = "전화번호";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 65);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 38);
            this.label2.TabIndex = 2;
            this.label2.Text = "이름";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 24);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 38);
            this.label1.TabIndex = 1;
            this.label1.Text = "코드";
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(308, 276);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(82, 29);
            this.btn_add.TabIndex = 13;
            this.btn_add.Text = "저장";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // PartnerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(22F, 38F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 314);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "PartnerForm";
            this.Text = "파트너 - 사용보기";
            this.Load += new System.EventHandler(this.PartnerForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView lv_partnerList;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.TextBox tb_partnerCode;
        private System.Windows.Forms.TextBox tb_partnerName;
        private System.Windows.Forms.TextBox tb_parterPhoneNumber;
        private System.Windows.Forms.TextBox tb_partnerMemo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 보기ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 전체ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 사용ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 미사용ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 사용ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 미사용ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 새로고침F5ToolStripMenuItem;
        private System.Windows.Forms.Button btn_add;
    }
}