﻿using OrderProgram.Common;
using OrderProgram.Model;
using OrderProgram.MyControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static OrderProgram.MainForm;
using static OrderProgram.Model.HospitalOrder;

namespace OrderProgram
{
    public partial class OrderForm : Form
    {
        public delegate void AddHospitalOrder(HospitalOrder hospitalOrder);
        public delegate void UpdateHospitalOrder(HospitalOrder hospitalOrder);
        public event AddHospitalOrder AddCallback;
        public event UpdateHospitalOrder UpdateCallback;

        private Partner SelectPartner = null;
        private Hospital SelectHospital = null;
        private SelectModel SelectOrder = null;
        private bool FirstData = true;
        private HospitalOrder HospitalOrder = new HospitalOrder();
        private bool UpdateMode = false;
        
        public HospitalOrder SelectHospitalOrder { get; internal set; }

        public OrderForm(bool updateMode = false)
        {
            
            UpdateMode = updateMode;
            InitializeComponent();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            Keys key = keyData & ~(Keys.Shift | Keys.Control);

            switch (key)
            {
                case Keys.Delete:
                    {
                        DeleteOrder();
                        return true;
                    }
                case Keys.F5:
                    {
                        SelectOrder = null;
                        ClearOrder();
                        return true;
                    }
                case Keys.Escape:
                    {
                        Close();
                        return true;
                    }
                case Keys.Enter:
                    {
                        if ((keyData & Keys.Shift) != 0)
                            UpdateOrSaveOrder();
                        else if (tb_partner.Focused)
                            SearchFromCodeModel(new Partner());
                        else if (tb_hospital.Focused)
                            SearchFromCodeModel(new Hospital());
                        else if (tb_box.Focused || tb_price.Focused)
                            NextTab();
                        else if (button1.Focused)
                            AddOrder();
                        return true;
                    }
                default:
                    return base.ProcessCmdKey(ref msg, keyData);
            }
        }

        private void AddOrder()
        {
            if(SelectOrder != null)
            {
                Order nowOrder = SelectOrder.CodeModel as Order;
                if (MessageBox.Show($"{nowOrder.OrderKind},{nowOrder.LargeCategory.NAME},{nowOrder.SmallCategory.NAME}," +
                    $"{nowOrder.Box},{nowOrder.Price}" +
                    $"\n수정하시겠습니까?","질문",MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    MemoryControl.UpdateOrder(nowOrder);
                }
                return;
            }
            if(SelectHospital == null || SelectPartner == null)
            {
                MessageBox.Show("병원 또는 요청자를 선택하지 않으셨습니다.");
                return;
            }

            if(FirstData)
            {
                HospitalOrder hospitalOrder = 
                    new HospitalOrder(dateTimePicker1.Value.ToString("yyyy/MM/dd HH:mm:ss"), SelectHospital, SelectPartner);
                HospitalOrder = hospitalOrder;
                DBControl.Instance.Add(HospitalOrder);
                HospitalOrder.Sequence = DBControl.Instance.GetHOSequence(hospitalOrder);
                FirstData = false;
                HospitalAndPartnerEnable(false);
            }

            MyCommon.ORDER_KIND kind = GetOrderKind();
            Category largeCategory = comboBox1.SelectedItem as Category;
            Category smallCategory = comboBox2.SelectedItem as Category;
            int box = int.Parse(tb_box.Text);
            long price = long.Parse(tb_price.Text);
            CheckValue(kind, largeCategory, smallCategory);
            long surtax = 0;
            if (cb_부가세.Checked)
                surtax = price / 10;
            Order order = new Order(HospitalOrder.Orders.Count + 1, HospitalOrder.Sequence, 
                kind, largeCategory, smallCategory, box, price, surtax);
            if (MessageBox.Show($"{order.OrderKind},{order.LargeCategory.NAME},{order.SmallCategory.NAME}," +
                    $"{order.Box},{order.Price}" +
                    $"\n추가하시겠습니까?", "질문", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                HospitalOrder.Orders.Add(order);
                MyListViewItem listViewItem = new MyListViewItem(order);
                lv_order.Items.Add(listViewItem);
                ClearOrder();
            }
            
        }

        private void ClearOrder()
        {
            cb_부가세.Checked = false;
            tb_box.Text = string.Empty;
            tb_price.Text = string.Empty;
            comboBox1.SelectedIndex = -1;
            comboBox2.SelectedIndex = -1;
            comboBox2.Items.Clear();
            tb_shortcut.Focus();
        }

        private void CheckValue(MyCommon.ORDER_KIND kind, Category largeCategory, Category smallCategory)
        {
            if (kind == MyCommon.ORDER_KIND.NONE)
            {
                MessageBox.Show("종류를 선택해 주세요.");
                tb_shortcut.Focus();
                return;
            }
            if (largeCategory == null)
            {
                MessageBox.Show("대분류를 선택해주세요.");
                tb_large.Focus();
                return;
            }
            if (smallCategory == null)
            {
                MessageBox.Show("소분류를 선택해주세요.");
                tb_small.Focus();
                return;
            }
            if (tb_box.Text == string.Empty)
            {
                MessageBox.Show("박스를 입력해주세요.");
                tb_box.Focus();
                return;
            }
            if (tb_price.Text == string.Empty)
            {
                MessageBox.Show("박스를 입력해주세요.");
                tb_price.Focus();
                return;
            }
            return;
        }

        private MyCommon.ORDER_KIND GetOrderKind()
        {
            foreach(Control control in groupBox1.Controls)
            {
                if (control is RadioButton)
                {
                    RadioButton radioButton = control as RadioButton;
                    foreach (MyCommon.ORDER_KIND kind in (MyCommon.ORDER_KIND[])Enum.GetValues(typeof(MyCommon.ORDER_KIND)))
                    {
                        if (radioButton.Checked &&
                            radioButton.Name.Contains(kind.ToString()))
                        {
                            return kind;
                        }
                    }
                }
            }
            return MyCommon.ORDER_KIND.NONE;
        }

        private void HospitalAndPartnerEnable(bool Enabled)
        {
            tb_hospital.Enabled = Enabled;
            tb_partner.Enabled = Enabled;
            btn_detail.Enabled = Enabled;
            btn_hospitalSearch.Enabled = Enabled;
            btn_partnerSearch.Enabled = Enabled;
            dateTimePicker1.Enabled = Enabled;
        }

        private void UpdateOrSaveOrder()
        {
            if(UpdateMode)
            {
                UpdateOrder();
            }else
            {
                SaveOrder();
            }
        }

        private void UpdateOrder()
        {
            DBControl.Instance.DeleteOrders(HospitalOrder);
            DBControl.Instance.AddOrder(HospitalOrder);
            UpdateCallback(HospitalOrder);
            Close();
        }

        private void SaveOrder()
        {
            DBControl.Instance.AddOrder(HospitalOrder);
            AddCallback(HospitalOrder);
            Close();
        }

        private void btn_hospitalSearch_Click(object sender, EventArgs e)
        {
            SearchFromCodeModel(new Hospital());
        }

        private void SearchFromCodeModel(CodeModel codeModel)
        {
            SelectForm selectForm = new SelectForm(codeModel);
            TextBox textBox = null;
            if (codeModel is Hospital)
                textBox = tb_hospital;
            else if(codeModel is Partner)
                textBox = tb_partner;

            if (textBox.Text != string.Empty)
                selectForm.fillter = textBox.Text;

            selectForm.StartPosition = FormStartPosition.CenterScreen;
            selectForm.SelectItem += SelectForm_SelectItem;
            selectForm.ShowDialog();

            if (selectForm.DialogResult == DialogResult.Retry)
            {
                MessageBox.Show("검색실패");
                textBox.Text = string.Empty;
            }
            else if (selectForm.DialogResult == DialogResult.OK)
                NextTab();
            else
                MessageBox.Show("선택취소");


        }

        private void SelectForm_SelectItem(CodeModel codeModel)
        {
            if(codeModel is Hospital)
            {
                this.SelectHospital = codeModel as Hospital;
                tb_hospital.Text = SelectHospital.NAME;
            }
            else if (codeModel is Partner)
            {
                this.SelectPartner = codeModel as Partner;
                tb_partner.Text = SelectPartner.NAME;
                
            }
        }

        private void btn_partnerSearch_Click(object sender, EventArgs e)
        {
            SearchFromCodeModel(new Partner());
        }

        private void NextTab() => SendKeys.Send("{tab}");
        private void SelectShortCut(RadioButton radioButton)
        {
            radioButton.Checked = true;
            tb_shortcut.Text = string.Empty;
            cb_부가세.Focus();
        }

        private void tb_shortcut_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.D1:
                    {
                        SelectShortCut(rb_유상);
                        break;
                    }
                case Keys.D2:
                    {
                        SelectShortCut(rb_무상);
                        break;
                    }
                case Keys.D3:
                    {
                        SelectShortCut(rb_서비스);
                        break;
                    }
                default:
                    {
                        tb_shortcut.Text = string.Empty;
                        break;
                    }

            }
        }

        private void OrderForm_Load(object sender, EventArgs e)
        {
            SetCategories();
            if (UpdateMode)
                ChangeUpdateForm();
        }
        

        private void ChangeUpdateForm()
        {
            FirstData = false;
            HospitalAndPartnerEnable(false);
            this.HospitalOrder = SelectHospitalOrder;
            SelectHospital = SelectHospitalOrder.Hospital;
            tb_hospital.Text = SelectHospital.NAME;
            SelectPartner = SelectHospitalOrder.Partner;
            tb_partner.Text = SelectPartner.NAME;
            foreach (Order order in SelectHospitalOrder.Orders)
            {
                MyListViewItem myListViewItem = new MyListViewItem(order);
                lv_order.Items.Add(myListViewItem);
            }

            if (SelectHospitalOrder.Orders.Count != 0)
            {
                lv_order.Items[0].Selected = true;
            }
        }

        private void Lv_order_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lv_order.SelectedItems.Count != 0)
            {
                MyListViewItem myListViewItem = (MyListViewItem)lv_order.SelectedItems[0];
                Order order = myListViewItem.CodeModel as Order;
                if (SelectOrder == null)
                    SelectOrder = new SelectModel(order, lv_order.SelectedIndices[0]);
                else
                    (SelectOrder.CodeModel, SelectOrder.SelectIndex) = (order, lv_order.SelectedIndices[0]);
                SelectKind(order.OrderKind);
                comboBox1.SelectedItem = order.LargeCategory;
                comboBox1_SelectedIndexChanged(order.SmallCategory, null);
                tb_box.Text = order.Box.ToString();
                tb_price.Text = order.Price.ToString();
                if (order.Surtax != 0)
                    cb_부가세.Checked = true;
            }
        }

        private void SelectKind(MyCommon.ORDER_KIND orderKind)
        {
            foreach(Control control in groupBox1.Controls)
            {
                if(control is RadioButton)
                {
                    if(control.Name.Contains(orderKind.ToString()))
                    {
                        ((RadioButton)control).Checked = true;
                        return;
                    }
                }
            }
        }

        private void SetCategories()
        {
            var list = MemoryControl.GetCategories(MyCommon.CATEGORY_KIND.대분류);
            foreach (var value in list)
            {
                comboBox1.Items.Add(value);
            }
         }
        private void SelectCategory(object sender, string data)
        {
            TextBox textBox = sender as TextBox;
            int selectComboBox = int.Parse(data);
            ComboBox comboBox = (textBox.Name == "tb_large") ? comboBox1 : comboBox2;
            try
            {
                comboBox.SelectedIndex = selectComboBox - 1;
            }
            catch (Exception)
            {
                comboBox.SelectedIndex = comboBox.Items.Count - 1;
            }
            NextTab();
        }
        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar)))
                e.Handled = true;
            SelectCategory(sender, e.KeyChar.ToString());
            e.Handled = true;
        }

        private void tb_shortcut_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar)))
                e.Handled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddOrder();
        }

        private void tb_box_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsDigit(e.KeyChar) || e.KeyChar == Convert.ToChar(Keys.Back)))
                e.Handled = true;
        }

        private void cb_부가세_CheckedChanged(object sender, EventArgs e)
        {
            tb_large.Focus();
        }

        private void 삭제ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DeleteOrder();
        }

        private void DeleteOrder()
        {
            Order nowOrder = SelectOrder.CodeModel as Order;
            if (MessageBox.Show($"{nowOrder.OrderKind},{nowOrder.LargeCategory.NAME},{nowOrder.SmallCategory.NAME}," +
                    $"{nowOrder.Box},{nowOrder.Price}" +
                    $"\n삭제하시겠습니까?", "질문", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                MemoryControl.DeleteOrder(nowOrder);
                lv_order.Items.RemoveAt(SelectOrder.SelectIndex);
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            UpdateOrSaveOrder();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem != null)
            {
                comboBox2.Items.Clear();
                Category category = comboBox1.SelectedItem as Category;
                var list = DBControl.Instance.GetCategoryTree(category.CODE);
                foreach (Category small in list)
                {
                    comboBox2.Items.Add(small);
                    if (sender is Category)
                    {
                        Category cate = sender as Category;
                        if (small.CODE == cate.CODE)    
                            comboBox2.SelectedItem = small;
                    }
                }

                
            }
        }
    }
}
