﻿namespace OrderProgram
{
    partial class StatsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btn_search = new System.Windows.Forms.Button();
            this.cb_hospital = new System.Windows.Forms.ComboBox();
            this.gb_kind = new System.Windows.Forms.GroupBox();
            this.cb_all = new System.Windows.Forms.CheckBox();
            this.cb_서비스 = new System.Windows.Forms.CheckBox();
            this.cb_무상 = new System.Windows.Forms.CheckBox();
            this.cb_유상 = new System.Windows.Forms.CheckBox();
            this.gb_class = new System.Windows.Forms.GroupBox();
            this.cb_class = new System.Windows.Forms.ComboBox();
            this.rb_small = new System.Windows.Forms.RadioButton();
            this.rb_large = new System.Windows.Forms.RadioButton();
            this.listView1 = new System.Windows.Forms.ListView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.gb_kind.SuspendLayout();
            this.gb_class.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btn_search);
            this.splitContainer1.Panel1.Controls.Add(this.cb_hospital);
            this.splitContainer1.Panel1.Controls.Add(this.gb_kind);
            this.splitContainer1.Panel1.Controls.Add(this.gb_class);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.listView1);
            this.splitContainer1.Size = new System.Drawing.Size(1083, 632);
            this.splitContainer1.SplitterDistance = 76;
            this.splitContainer1.SplitterWidth = 11;
            this.splitContainer1.TabIndex = 0;
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(991, 27);
            this.btn_search.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(84, 28);
            this.btn_search.TabIndex = 3;
            this.btn_search.Text = "검색";
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // cb_hospital
            // 
            this.cb_hospital.FormattingEnabled = true;
            this.cb_hospital.Location = new System.Drawing.Point(704, 30);
            this.cb_hospital.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.cb_hospital.Name = "cb_hospital";
            this.cb_hospital.Size = new System.Drawing.Size(283, 24);
            this.cb_hospital.TabIndex = 2;
            // 
            // gb_kind
            // 
            this.gb_kind.Controls.Add(this.cb_all);
            this.gb_kind.Controls.Add(this.cb_서비스);
            this.gb_kind.Controls.Add(this.cb_무상);
            this.gb_kind.Controls.Add(this.cb_유상);
            this.gb_kind.Location = new System.Drawing.Point(453, 9);
            this.gb_kind.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.gb_kind.Name = "gb_kind";
            this.gb_kind.Padding = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.gb_kind.Size = new System.Drawing.Size(246, 53);
            this.gb_kind.TabIndex = 1;
            this.gb_kind.TabStop = false;
            this.gb_kind.Text = "종류";
            // 
            // cb_all
            // 
            this.cb_all.AutoSize = true;
            this.cb_all.Location = new System.Drawing.Point(48, 1);
            this.cb_all.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cb_all.Name = "cb_all";
            this.cb_all.Size = new System.Drawing.Size(15, 14);
            this.cb_all.TabIndex = 3;
            this.cb_all.UseVisualStyleBackColor = true;
            this.cb_all.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // cb_서비스
            // 
            this.cb_서비스.AutoSize = true;
            this.cb_서비스.Location = new System.Drawing.Point(166, 23);
            this.cb_서비스.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.cb_서비스.Name = "cb_서비스";
            this.cb_서비스.Size = new System.Drawing.Size(75, 20);
            this.cb_서비스.TabIndex = 2;
            this.cb_서비스.Text = "서비스";
            this.cb_서비스.UseVisualStyleBackColor = true;
            this.cb_서비스.CheckedChanged += new System.EventHandler(this.cb_서비스_CheckedChanged_1);
            // 
            // cb_무상
            // 
            this.cb_무상.AutoSize = true;
            this.cb_무상.Location = new System.Drawing.Point(91, 23);
            this.cb_무상.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.cb_무상.Name = "cb_무상";
            this.cb_무상.Size = new System.Drawing.Size(59, 20);
            this.cb_무상.TabIndex = 1;
            this.cb_무상.Text = "무상";
            this.cb_무상.UseVisualStyleBackColor = true;
            this.cb_무상.CheckedChanged += new System.EventHandler(this.cb_무상_CheckedChanged);
            // 
            // cb_유상
            // 
            this.cb_유상.AutoSize = true;
            this.cb_유상.Location = new System.Drawing.Point(16, 23);
            this.cb_유상.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.cb_유상.Name = "cb_유상";
            this.cb_유상.Size = new System.Drawing.Size(59, 20);
            this.cb_유상.TabIndex = 0;
            this.cb_유상.Text = "유상";
            this.cb_유상.UseVisualStyleBackColor = true;
            this.cb_유상.CheckedChanged += new System.EventHandler(this.cb_유상_CheckedChanged);
            // 
            // gb_class
            // 
            this.gb_class.Controls.Add(this.cb_class);
            this.gb_class.Controls.Add(this.rb_small);
            this.gb_class.Controls.Add(this.rb_large);
            this.gb_class.Location = new System.Drawing.Point(8, 9);
            this.gb_class.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.gb_class.Name = "gb_class";
            this.gb_class.Padding = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.gb_class.Size = new System.Drawing.Size(436, 53);
            this.gb_class.TabIndex = 0;
            this.gb_class.TabStop = false;
            this.gb_class.Text = "분류";
            // 
            // cb_class
            // 
            this.cb_class.FormattingEnabled = true;
            this.cb_class.Location = new System.Drawing.Point(190, 21);
            this.cb_class.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.cb_class.Name = "cb_class";
            this.cb_class.Size = new System.Drawing.Size(239, 24);
            this.cb_class.TabIndex = 2;
            // 
            // rb_small
            // 
            this.rb_small.AutoSize = true;
            this.rb_small.Location = new System.Drawing.Point(106, 22);
            this.rb_small.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.rb_small.Name = "rb_small";
            this.rb_small.Size = new System.Drawing.Size(74, 20);
            this.rb_small.TabIndex = 1;
            this.rb_small.Text = "소분류";
            this.rb_small.UseVisualStyleBackColor = true;
            this.rb_small.Click += new System.EventHandler(this.rb_small_Click);
            // 
            // rb_large
            // 
            this.rb_large.AutoSize = true;
            this.rb_large.Checked = true;
            this.rb_large.Location = new System.Drawing.Point(16, 22);
            this.rb_large.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.rb_large.Name = "rb_large";
            this.rb_large.Size = new System.Drawing.Size(74, 20);
            this.rb_large.TabIndex = 0;
            this.rb_large.TabStop = true;
            this.rb_large.Text = "대분류";
            this.rb_large.UseVisualStyleBackColor = true;
            this.rb_large.Click += new System.EventHandler(this.rb_large_Click);
            // 
            // listView1
            // 
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.FullRowSelect = true;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1083, 545);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // StatsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1083, 632);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.Name = "StatsForm";
            this.Text = "통계";
            this.Load += new System.EventHandler(this.StatsForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.gb_kind.ResumeLayout(false);
            this.gb_kind.PerformLayout();
            this.gb_class.ResumeLayout(false);
            this.gb_class.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.GroupBox gb_class;
        private System.Windows.Forms.ComboBox cb_class;
        private System.Windows.Forms.RadioButton rb_small;
        private System.Windows.Forms.RadioButton rb_large;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.GroupBox gb_kind;
        private System.Windows.Forms.CheckBox cb_서비스;
        private System.Windows.Forms.CheckBox cb_무상;
        private System.Windows.Forms.CheckBox cb_유상;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.ComboBox cb_hospital;
        private System.Windows.Forms.CheckBox cb_all;
    }
}