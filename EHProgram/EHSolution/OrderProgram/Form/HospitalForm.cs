﻿using OrderProgram.Common;
using OrderProgram.MyControl;
using OrderProgram.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrderProgram
{
    public partial class HospitalForm : Form
    {
        private static MyCommon.FLAG? NowFlag = null;
        public HospitalForm()
        {
            InitializeComponent();
            InitializeControl();
        }

        private void InitializeControl()
        {
            this.ActiveControl = tb_hospitalCode;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            Keys key = keyData & ~(Keys.Shift | Keys.Control);

            switch (key)
            {
                case Keys.Enter:
                    {
                        if ((keyData & Keys.Shift) != 0)
                            UpdateOrAddHospital();
                        return true;
                    }
                case Keys.Escape:
                    {
                        Close();
                        return true;
                    }
                case Keys.Delete:
                    {
                        DeleteHospital();
                        return true;
                    }
                case Keys.F5:
                    {
                        ClearTextBox();
                        return true;
                    }
                default:
                    return base.ProcessCmdKey(ref msg, keyData);
            }
        }

        private void UpdateOrAddHospital()
        {
            string code = tb_hospitalCode.Text;
            if (code == string.Empty) { MessageBox.Show("코드 미입력."); tb_hospitalCode.Focus(); return; }
            string name = tb_hospitalName.Text;
            string manager = tb_manager.Text;
            string phone = tb_hospitalPhoneNumber.Text;
            string address = tb_hospitaddress.Text;
            string memo = tb_hospitalMemo.Text;

            Hospital hospital = new Hospital(code, name, manager, phone, address, memo, NowFlag);

            if (MemoryControl.CompareCode(hospital))
                UpdateHospital(hospital);
            else
                AddHospital(hospital);
            


        }

        private void UpdateHospital(Hospital hospital)
        {
            DBControl.Instance.Update(hospital);
            ReflashList();
        }

        private void DeleteHospital()
        {
            if (lv_hospitalList.SelectedItems != null)
            {
                if (NowFlag == MyCommon.FLAG.NONUSING)
                {
                    if(MessageBox.Show($"=============================\nCode: {lv_hospitalList.SelectedItems[0].SubItems[0].Text}\nName: {lv_hospitalList.SelectedItems[0].SubItems[1].Text}\n=============================\n삭제하시겠습니까?","질문", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        MyListViewItem myListViewItem = (MyListViewItem)lv_hospitalList.SelectedItems[0];
                        DBControl.Instance.Delete(myListViewItem.CodeModel);
                        ReflashList();
                        ClearTextBox();
                    }
                }
                else
                {
                    if (MessageBox.Show("미사용보기 상태에서만 삭제가 가능합니다.\n미사용보기로 변경하시겠습니까?",
                        "질문", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        SetHospitalList(MyCommon.FLAG.NONUSING);
                    };
                }
            }
        }

        private void AddHospital(Hospital hospital)
        {
            lv_hospitalList.Items.Add(new MyListViewItem(hospital));
            DBControl.Instance.Add(hospital);
            ClearTextBox();
        }

        private void ClearTextBox()
        {
            tb_hospitalCode.Enabled = true;
            tb_hospitalCode.Text = string.Empty;
            tb_hospitalName.Text = string.Empty;
            tb_manager.Text = string.Empty;
            tb_hospitalPhoneNumber.Text = string.Empty;
            tb_hospitaddress.Text = string.Empty;
            tb_hospitalMemo.Text = string.Empty;
            tb_hospitalCode.Focus();
        }

        private void HospitalForm_Load(object sender, EventArgs e)
        {
            SetHospitalList(MyCommon.FLAG.USING);
        }

        private void SetHospitalList(MyCommon.FLAG? flag)
        {
            lv_hospitalList.Items.Clear();
            var hospitals = MemoryControl.GetHospitals();
            foreach(Hospital hospital in hospitals)
            {
                if (hospital.FLAG == flag)
                    lv_hospitalList.Items.Add(new MyListViewItem(hospital));
                else if (flag == MyCommon.FLAG.ALL)
                {
                    MyListViewItem lvi = lvi = new MyListViewItem(hospital);
                    if (hospital.FLAG == MyCommon.FLAG.NONUSING)
                        lvi.BackColor = Color.Yellow;

                    lv_hospitalList.Items.Add(lvi);
                }
            }
            NowFlag = flag;
        }

        private void tb_hospitalCode_Leave(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if(textBox.Text != string.Empty)
            {
                CodeModel codeModel = new Hospital();
                codeModel.CODE = textBox.Text;
                if (MemoryControl.CompareCode(codeModel))
                {
                    MessageBox.Show("코드 중복");
                    textBox.Focus();
                }
            }
        }

        private void 전체ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetHospitalList(MyCommon.FLAG.ALL);
            this.Text = "병원 - 전체보기";
        }

        private void 사용ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SetHospitalList(MyCommon.FLAG.USING);
            this.Text = "병원 - 사용보기";
        }

        private void 미사용ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SetHospitalList(MyCommon.FLAG.NONUSING);
            this.Text = "병원 - 미사용보기";
        }

        private void 사용ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MyListViewItem myListViewItem = (MyListViewItem)lv_hospitalList.SelectedItems[0];
            DBControl.Instance.UpdateUsing(myListViewItem.CodeModel);
            ReflashList();
        }

        private void ReflashList()
        {
            SetHospitalList(NowFlag);
        }

        private void 미사용ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MyListViewItem myListViewItem = (MyListViewItem)lv_hospitalList.SelectedItems[0];
            DBControl.Instance.UpdateUsing(myListViewItem.CodeModel);
            ReflashList();
        }

        private void tb_hospitaddress_Enter(object sender, EventArgs e)
        {
            if(tb_hospitaddress.Text == string.Empty)
            {
                DaumAPIForm daumAPIForm = new DaumAPIForm();
                daumAPIForm.StartPosition = FormStartPosition.CenterScreen;
                daumAPIForm.FormSendEvent += DaumAPIForm_FormSendEvent;
                daumAPIForm.Show();
            }
        }

        private void DaumAPIForm_FormSendEvent(string address)
        {
            tb_hospitaddress.Text = address;
            tb_hospitaddress.Select(tb_hospitaddress.Text.Length, tb_hospitaddress.Text.Length);
        }

        private void 새로고침F5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearTextBox();
        }

        private void lv_hospitalList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lv_hospitalList.SelectedItems.Count != 0)
            {
                MyListViewItem myListViewItem = (MyListViewItem)lv_hospitalList.SelectedItems[0];
                Hospital hospital = MemoryControl.GetModelFromCode(myListViewItem.CodeModel) as Hospital;
                SetTextBoxFromHospital(hospital);
            }
        }

        private void SetTextBoxFromHospital(Hospital hospital)
        {
            tb_hospitalCode.Enabled = false;
            tb_hospitalCode.Text = hospital.CODE;
            tb_hospitalName.Text = hospital.NAME;
            tb_manager.Text = hospital.MANAGER;
            tb_hospitalPhoneNumber.Text = hospital.PHONE;
            tb_hospitaddress.Text = hospital.ADDRESS;
            tb_hospitalMemo.Text = hospital.MEMO;
            tb_hospitalName.Focus();
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            UpdateOrAddHospital();
        }
    }


    
}
