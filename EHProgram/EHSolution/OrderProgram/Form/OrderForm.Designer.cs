﻿namespace OrderProgram
{
    partial class OrderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_detail = new System.Windows.Forms.Button();
            this.btn_partnerSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_partner = new System.Windows.Forms.TextBox();
            this.btn_hospitalSearch = new System.Windows.Forms.Button();
            this.tb_hospital = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.cb_부가세 = new System.Windows.Forms.CheckBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_small = new System.Windows.Forms.TextBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tb_large = new System.Windows.Forms.TextBox();
            this.tb_price = new System.Windows.Forms.TextBox();
            this.tb_box = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rb_무상 = new System.Windows.Forms.RadioButton();
            this.rb_서비스 = new System.Windows.Forms.RadioButton();
            this.tb_shortcut = new System.Windows.Forms.TextBox();
            this.rb_유상 = new System.Windows.Forms.RadioButton();
            this.lv_order = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.삭제ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.Info;
            this.splitContainer1.Panel1.Controls.Add(this.btn_save);
            this.splitContainer1.Panel1.Controls.Add(this.btn_detail);
            this.splitContainer1.Panel1.Controls.Add(this.btn_partnerSearch);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.tb_partner);
            this.splitContainer1.Panel1.Controls.Add(this.btn_hospitalSearch);
            this.splitContainer1.Panel1.Controls.Add(this.tb_hospital);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Menu;
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(703, 308);
            this.splitContainer1.SplitterDistance = 53;
            this.splitContainer1.SplitterWidth = 6;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.TabStop = false;
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(636, 13);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(55, 29);
            this.btn_save.TabIndex = 17;
            this.btn_save.TabStop = false;
            this.btn_save.Text = "저장";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_detail
            // 
            this.btn_detail.Location = new System.Drawing.Point(265, 10);
            this.btn_detail.Name = "btn_detail";
            this.btn_detail.Size = new System.Drawing.Size(81, 29);
            this.btn_detail.TabIndex = 9;
            this.btn_detail.TabStop = false;
            this.btn_detail.Text = "자세히";
            this.btn_detail.UseVisualStyleBackColor = true;
            // 
            // btn_partnerSearch
            // 
            this.btn_partnerSearch.Location = new System.Drawing.Point(550, 10);
            this.btn_partnerSearch.Name = "btn_partnerSearch";
            this.btn_partnerSearch.Size = new System.Drawing.Size(35, 29);
            this.btn_partnerSearch.TabIndex = 10;
            this.btn_partnerSearch.TabStop = false;
            this.btn_partnerSearch.Text = "button1";
            this.btn_partnerSearch.UseVisualStyleBackColor = true;
            this.btn_partnerSearch.Click += new System.EventHandler(this.btn_partnerSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(357, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 38);
            this.label2.TabIndex = 3;
            this.label2.Text = "요청자";
            // 
            // tb_partner
            // 
            this.tb_partner.Location = new System.Drawing.Point(429, 10);
            this.tb_partner.Name = "tb_partner";
            this.tb_partner.Size = new System.Drawing.Size(115, 51);
            this.tb_partner.TabIndex = 2;
            // 
            // btn_hospitalSearch
            // 
            this.btn_hospitalSearch.Location = new System.Drawing.Point(224, 10);
            this.btn_hospitalSearch.Name = "btn_hospitalSearch";
            this.btn_hospitalSearch.Size = new System.Drawing.Size(35, 29);
            this.btn_hospitalSearch.TabIndex = 8;
            this.btn_hospitalSearch.TabStop = false;
            this.btn_hospitalSearch.Text = "button1";
            this.btn_hospitalSearch.UseVisualStyleBackColor = true;
            this.btn_hospitalSearch.Click += new System.EventHandler(this.btn_hospitalSearch_Click);
            // 
            // tb_hospital
            // 
            this.tb_hospital.Location = new System.Drawing.Point(103, 10);
            this.tb_hospital.Name = "tb_hospital";
            this.tb_hospital.Size = new System.Drawing.Size(115, 51);
            this.tb_hospital.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 38);
            this.label1.TabIndex = 0;
            this.label1.Text = "요청병원";
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.cb_부가세);
            this.splitContainer2.Panel1.Controls.Add(this.dateTimePicker1);
            this.splitContainer2.Panel1.Controls.Add(this.groupBox2);
            this.splitContainer2.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.lv_order);
            this.splitContainer2.Size = new System.Drawing.Size(703, 249);
            this.splitContainer2.SplitterDistance = 131;
            this.splitContainer2.TabIndex = 0;
            this.splitContainer2.TabStop = false;
            // 
            // cb_부가세
            // 
            this.cb_부가세.AutoSize = true;
            this.cb_부가세.Location = new System.Drawing.Point(206, 26);
            this.cb_부가세.Name = "cb_부가세";
            this.cb_부가세.Size = new System.Drawing.Size(252, 42);
            this.cb_부가세.TabIndex = 4;
            this.cb_부가세.Text = "부가세 포함";
            this.cb_부가세.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(424, 11);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(266, 51);
            this.dateTimePicker1.TabIndex = 1;
            this.dateTimePicker1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.tb_small);
            this.groupBox2.Controls.Add(this.comboBox2);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.tb_large);
            this.groupBox2.Controls.Add(this.tb_price);
            this.groupBox2.Controls.Add(this.tb_box);
            this.groupBox2.Location = new System.Drawing.Point(12, 68);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(684, 54);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "정보";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(624, 17);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(55, 29);
            this.button1.TabIndex = 9;
            this.button1.Text = "추가";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(467, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 38);
            this.label4.TabIndex = 15;
            this.label4.Text = "가격";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(316, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 38);
            this.label3.TabIndex = 14;
            this.label3.Text = "Box";
            // 
            // tb_small
            // 
            this.tb_small.Location = new System.Drawing.Point(163, 19);
            this.tb_small.Name = "tb_small";
            this.tb_small.Size = new System.Drawing.Size(24, 51);
            this.tb_small.TabIndex = 6;
            this.tb_small.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox4_KeyPress);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(193, 19);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 46);
            this.comboBox2.TabIndex = 16;
            this.comboBox2.TabStop = false;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(36, 19);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 46);
            this.comboBox1.TabIndex = 15;
            this.comboBox1.TabStop = false;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // tb_large
            // 
            this.tb_large.Location = new System.Drawing.Point(6, 19);
            this.tb_large.Name = "tb_large";
            this.tb_large.Size = new System.Drawing.Size(24, 51);
            this.tb_large.TabIndex = 5;
            this.tb_large.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox4_KeyPress);
            // 
            // tb_price
            // 
            this.tb_price.Location = new System.Drawing.Point(520, 17);
            this.tb_price.Name = "tb_price";
            this.tb_price.Size = new System.Drawing.Size(100, 51);
            this.tb_price.TabIndex = 8;
            // 
            // tb_box
            // 
            this.tb_box.Location = new System.Drawing.Point(361, 18);
            this.tb_box.Name = "tb_box";
            this.tb_box.Size = new System.Drawing.Size(100, 51);
            this.tb_box.TabIndex = 7;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rb_무상);
            this.groupBox1.Controls.Add(this.rb_서비스);
            this.groupBox1.Controls.Add(this.tb_shortcut);
            this.groupBox1.Controls.Add(this.rb_유상);
            this.groupBox1.Location = new System.Drawing.Point(12, 8);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(188, 54);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "종류";
            // 
            // rb_무상
            // 
            this.rb_무상.AutoSize = true;
            this.rb_무상.Location = new System.Drawing.Point(88, 20);
            this.rb_무상.Name = "rb_무상";
            this.rb_무상.Size = new System.Drawing.Size(86, 42);
            this.rb_무상.TabIndex = 99;
            this.rb_무상.Text = "무";
            this.rb_무상.UseVisualStyleBackColor = true;
            // 
            // rb_서비스
            // 
            this.rb_서비스.AutoSize = true;
            this.rb_서비스.Location = new System.Drawing.Point(140, 20);
            this.rb_서비스.Name = "rb_서비스";
            this.rb_서비스.Size = new System.Drawing.Size(86, 42);
            this.rb_서비스.TabIndex = 99;
            this.rb_서비스.Text = "서";
            this.rb_서비스.UseVisualStyleBackColor = true;
            // 
            // tb_shortcut
            // 
            this.tb_shortcut.Location = new System.Drawing.Point(6, 19);
            this.tb_shortcut.Name = "tb_shortcut";
            this.tb_shortcut.Size = new System.Drawing.Size(24, 51);
            this.tb_shortcut.TabIndex = 3;
            this.tb_shortcut.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tb_shortcut_KeyUp);
            // 
            // rb_유상
            // 
            this.rb_유상.AutoSize = true;
            this.rb_유상.Location = new System.Drawing.Point(36, 20);
            this.rb_유상.Name = "rb_유상";
            this.rb_유상.Size = new System.Drawing.Size(86, 42);
            this.rb_유상.TabIndex = 99;
            this.rb_유상.Text = "유";
            this.rb_유상.UseVisualStyleBackColor = true;
            // 
            // lv_order
            // 
            this.lv_order.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.lv_order.ContextMenuStrip = this.contextMenuStrip1;
            this.lv_order.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv_order.FullRowSelect = true;
            this.lv_order.Location = new System.Drawing.Point(0, 0);
            this.lv_order.MultiSelect = false;
            this.lv_order.Name = "lv_order";
            this.lv_order.Size = new System.Drawing.Size(703, 114);
            this.lv_order.TabIndex = 0;
            this.lv_order.UseCompatibleStateImageBehavior = false;
            this.lv_order.View = System.Windows.Forms.View.Details;
            this.lv_order.SelectedIndexChanged += new System.EventHandler(this.Lv_order_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "순번";
            this.columnHeader1.Width = 52;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "종류";
            this.columnHeader2.Width = 50;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "대분류";
            this.columnHeader3.Width = 119;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "소분류";
            this.columnHeader4.Width = 113;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "박스";
            this.columnHeader5.Width = 75;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "가격";
            this.columnHeader6.Width = 80;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "부가세";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.삭제ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(139, 40);
            // 
            // 삭제ToolStripMenuItem
            // 
            this.삭제ToolStripMenuItem.Name = "삭제ToolStripMenuItem";
            this.삭제ToolStripMenuItem.Size = new System.Drawing.Size(138, 36);
            this.삭제ToolStripMenuItem.Text = "삭제";
            // 
            // OrderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(22F, 38F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 308);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "OrderForm";
            this.Text = "발주";
            this.Load += new System.EventHandler(this.OrderForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_hospitalSearch;
        private System.Windows.Forms.TextBox tb_hospital;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_partnerSearch;
        private System.Windows.Forms.TextBox tb_partner;
        private System.Windows.Forms.Button btn_detail;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_small;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox tb_large;
        private System.Windows.Forms.TextBox tb_price;
        private System.Windows.Forms.TextBox tb_box;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rb_무상;
        private System.Windows.Forms.RadioButton rb_서비스;
        private System.Windows.Forms.TextBox tb_shortcut;
        private System.Windows.Forms.RadioButton rb_유상;
        private System.Windows.Forms.ListView lv_order;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.CheckBox cb_부가세;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 삭제ToolStripMenuItem;
        private System.Windows.Forms.Button btn_save;
    }
}