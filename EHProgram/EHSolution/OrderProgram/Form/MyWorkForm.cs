﻿using OrderProgram.Model;
using OrderProgram.MyControl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrderProgram
{
    public partial class MyWorkForm : Form
    {
        public delegate void AddCallBack(MyWork myWork);
        public event AddCallBack MyWorkAddCallBack;
        public event AddCallBack MyWorkUpdateCallBack;
        private readonly int mY = 74;
        private static Point InitLabelLocation = new Point(8, 151);
        private static Point InitTextBoxLocation = new Point(61, 148);
        private Point NowLableLocation = InitLabelLocation;
        private Point NowTextBoxLocation = InitTextBoxLocation;
        private Size TextBoxSize = new Size(568, 64);
        private int memoNumber = 1;
        private Partner SelectPartner = new Partner();
        public MyWork SelectMyWork= null;
        public Stack<string> PreWorks = new Stack<string>();
        private bool UpdateMode = false;
        private TextBox LastTextBox = null;
        public MyWorkForm(bool updateMode = false)
        {
            InitializeComponent();
            InitializeContol(updateMode);
            UpdateMode = updateMode;
        }

        private void InitializeContol(bool updateMode)
        {
            this.ActiveControl = tb_partner;
                
        }

        private void ChangeUpdateForm()
        {
            this.Text = "내업무 - 수정";
            dateTimePicker1.Value = DateTime.Parse(SelectMyWork.RequestDate);
            dateTimePicker1.Enabled = false;
            SelectPartner = SelectMyWork.Partner;
            tb_partner.Text = SelectPartner.NAME;
            tb_partner.Enabled = false;
            btn_partnerSearch.Enabled = false;

            int idx = 0;
            foreach(string work in SelectMyWork.Works)
            {
                if (idx == 0)
                    tb_memo1.Text = work;
                else
                {
                    LastTextBox = CreateMemoBox();
                    LastTextBox.Text = work;
                }
                idx++;
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {

            Keys key = keyData & ~(Keys.Shift | Keys.Control);

            switch (key)
            {
                case Keys.Escape:
                    {
                        Close();
                        return true;
                    }
                case Keys.F2:
                case Keys.Oemplus:
                    {
                        LastTextBox = CreateMemoBox();
                        return true;
                    }
                case Keys.F3:
                case Keys.OemMinus:
                    {
                        RemoveMemoBox();
                        return true;
                    }
                case Keys.Enter:
                    {
                        if ((keyData & Keys.Shift) != 0)
                            UpdateOrAddMyWork();
                        else if (tb_partner.Focused)
                            SearchPartner();
                        return true;
                    }
                case Keys.F5:
                    {
                        ClearForm();
                        return true;
                    }
                default:
                    return base.ProcessCmdKey(ref msg, keyData);
            }
        }

        private void RemoveMemoBox()
        {
            if(memoNumber > 1)
            {
                NowLableLocation.Y = NowLableLocation.Y - mY;
                NowTextBoxLocation.Y = NowTextBoxLocation.Y - mY;
                this.Controls.RemoveByKey($"lb_memo{memoNumber}");
                this.Controls.RemoveByKey($"tb_memo{memoNumber}");
                this.Size = new Size(this.Size.Width, this.Size.Height - mY);
                memoNumber--;
                PreWorks.Push(LastTextBox.Text);
                CreateReturnButton();
            }
        }

        private void CreateReturnButton()
        {
            if (PreWorks.Count != 0)
            {
                ContextMenuStrip contextMenuStrip = new ContextMenuStrip();
                contextMenuStrip.Items.Add("되돌리기");
                contextMenuStrip.Items[0].Click += MyWorkForm_Click;
                this.ContextMenuStrip = contextMenuStrip;
            }
        }

        private void MyWorkForm_Click(object sender, EventArgs e)
        {
            LastTextBox = CreateMemoBox();
            LastTextBox.Text = PreWorks.Pop();
            if (PreWorks.Count == 0)
                this.ContextMenuStrip.Dispose();
        }

        private void UpdateOrAddMyWork()
        {
            string requestDate = $"{dateTimePicker1.Value:yyyy/MM/dd HH:mm:ss}";
            List<string> works = GetMyWorks();

            MyWork myWork = new MyWork(requestDate, SelectPartner, works);
            if (UpdateMode)
                UpdateMyWork(myWork);
            else
                AddMyWork(myWork);
            
        }

        private void AddMyWork(MyWork myWork)
        {
            DBControl.Instance.Add(myWork);
            MyWorkAddCallBack(myWork);
            Close();
        }

        private void UpdateMyWork(MyWork myWork)
        {
            myWork.RequestDate = SelectMyWork.RequestDate;
            myWork.Status = SelectMyWork.Status;
            DBControl.Instance.Update(myWork);
            MyWorkUpdateCallBack(myWork);
            Close();
        }

        private List<string> GetMyWorks()
        {
            List<string> myWorks = new List<string>();
            foreach(Control control in this.Controls)
            {
                if(control.Name.Contains("tb_memo"))
                {
                    TextBox textBox = control as TextBox;
                    myWorks.Add(textBox.Text);
                }
            }
            return myWorks;
        }

        private void ClearForm()
        {
            dateTimePicker1.Value = DateTime.Now;
            tb_partner.Text = string.Empty;
            InitMemoControls();
            tb_partner.Focus();
            SelectPartner = new Partner();
        }

        private void InitMemoControls()
        {
            List<Control> controls = GetMemoControls();
            foreach(var control in controls)
            {
                this.Controls.Remove(control);
            }
            NowLableLocation = InitLabelLocation;
            NowTextBoxLocation = InitTextBoxLocation;
            tb_memo1.Text = string.Empty;
            InitFormSize();
        }

        private void InitFormSize()
        {
            this.Size = new Size(663, 216);
        }

        private List<Control> GetMemoControls()
        {
            List<Control> controls = new List<Control>();
            foreach (Control control in this.Controls)
            {
                if ((control.Name.Contains("tb_memo") &&
                    !control.Name.Equals("tb_memo1")) || 
                    (control.Name.Contains("lb_memo") && 
                    !control.Name.Equals("lb_memo1")))
                {
                    controls.Add(control);
                }
            }
            return controls;
        }

        private TextBox CreateMemoBox()
        {
            //FormSize before = 663, 216 after 663, 290 answer 0, 74
            //Label Location before = 8, 77 after = 8, 151 answer 0, 74
            //TextBox Location before = 61, 74 after = 61, 148, answer 0, 74  Size 568, 64
            memoNumber++;
            Label label = new Label();
            label.Text = "메모";
            label.Name = $"lb_memo{memoNumber}";
            label.Location = NowLableLocation;
            label.Font = this.Font;
            label.AutoSize = true;
            
            TextBox textBox = new TextBox();
            textBox.Location = NowTextBoxLocation;
            textBox.Size = TextBoxSize;
            textBox.Font = this.Font;
            textBox.Multiline = true;
            textBox.Name = $"tb_memo{memoNumber}";

            NowLableLocation = new Point(label.Location.X, label.Location.Y + mY);
            NowTextBoxLocation = new Point(textBox.Location.X, textBox.Location.Y + mY);

            this.Size = new Size(this.Size.Width, this.Size.Height + mY);

            this.Controls.Add(label);
            this.Controls.Add(textBox);

            return textBox;
        }

        private void MyWorkForm_Load(object sender, EventArgs e)
        {
            if(UpdateMode)
                ChangeUpdateForm();
        }

        private void SearchPartner()
        {
            SelectForm selectForm = new SelectForm(new Partner());
            if (tb_partner.Text != string.Empty)
                selectForm.fillter = tb_partner.Text;
            selectForm.StartPosition = FormStartPosition.CenterScreen;
            selectForm.SelectItem += SelectForm_SelectItem;
            selectForm.ShowDialog();
            if (selectForm.DialogResult == DialogResult.Retry)
            {
                MessageBox.Show("검색실패");
                tb_partner.Text = string.Empty;
            }
            else if (selectForm.DialogResult == DialogResult.OK)
                tb_memo1.Focus();
            else
                MessageBox.Show("선택취소");
            
        }

        private void SelectForm_SelectItem(CodeModel codeModel)
        {
            this.SelectPartner = (Partner)codeModel;
            tb_partner.Text = SelectPartner.NAME;
        }

        private void btn_partnerSearch_Click(object sender, EventArgs e)
        {
            SearchPartner();

        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("작업을 저장하시겠습니까?", "질문", MessageBoxButtons.OKCancel) ==
                DialogResult.OK)
            {
                UpdateOrAddMyWork();
            }
        }

        private void 메모삭제ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RemoveMemoBox();
        }

        private void 메모추가ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LastTextBox = CreateMemoBox();
        }
        
    }
}
