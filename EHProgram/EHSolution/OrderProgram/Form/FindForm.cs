﻿using OrderProgram.Model;
using OrderProgram.MyControl;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

namespace OrderProgram
{
    
    public partial class FindForm : Form
    {
        public delegate void FindCodeModel(ArrayList codeModels);
        public event FindCodeModel FindCallBack;
        string[] MyJobKind = new string[] {"요청자" };
        string[] OrderKind = new string[] {"병원명", "요청자" };
        bool IsOrder = true;
        public FindForm(bool IsOrder)
        {
            this.IsOrder = IsOrder;
            InitializeComponent();
        }

        private void FindForm_Load(object sender, EventArgs e)
        {
            if (IsOrder)
                comboBox1.Items.AddRange(OrderKind);
            else
                comboBox1.Items.AddRange(MyJobKind);
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyData == Keys.Enter)
            {
                FindItems();
                Close();
            }
        }

        private void FindItems()
        {
            ArrayList list = new ArrayList();
            if(IsOrder)
            {
                List<HospitalOrder> hospitalOrders = MemoryControl.GetHospitalOrders();
                foreach(HospitalOrder hospitalOrder in hospitalOrders)
                {
                    if(comboBox1.SelectedIndex == 0
                        && hospitalOrder.Hospital.NAME.Contains(textBox1.Text))
                    {
                        list.Add(hospitalOrder);
                    }
                    if (comboBox1.SelectedIndex == 1
                        && hospitalOrder.Partner.NAME.Contains(textBox1.Text))
                    {
                        list.Add(hospitalOrder);
                    }
                }
            }else
            {
                List<MyWork> myWorks = MemoryControl.GetMyWorks();
                foreach (MyWork myWork in myWorks)
                {
                    if (comboBox1.SelectedIndex == 0
                        && myWork.Partner.NAME.Contains(textBox1.Text))
                    {
                        list.Add(myWork);
                    }
                }
            }
            FindCallBack(list);
        }
    }
}
