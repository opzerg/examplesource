﻿using OrderProgram.Common;
using OrderProgram.MyControl;
using OrderProgram.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrderProgram
{
    public partial class PartnerForm : Form
    {
        private static MyCommon.FLAG? NowFlag = null;
        public PartnerForm()
        {
            InitializeComponent();
            InitializeControl();
        }

        private void InitializeControl()
        {
            this.ActiveControl = tb_partnerCode;
        }
        

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            Keys key = keyData & ~(Keys.Shift | Keys.Control);

            switch(key)
            {
                case Keys.Enter:
                    {
                        if ((keyData & Keys.Shift) != 0)
                            UpdateOrAddPartner();
                        return true;
                    }
                case Keys.Escape:
                    {
                        Close();
                        return true;
                    }
                case Keys.Delete:
                    {
                        DeletePartner();
                        return true;
                    }
                case Keys.F5:
                    {
                        ClearTextBox();
                        return true;
                    }

                default:
                    return base.ProcessCmdKey(ref msg, keyData);
            }
            
        }

        private void DeletePartner()
        {
            if (lv_partnerList.SelectedItems != null)
            {
                if (NowFlag == MyCommon.FLAG.NONUSING)
                {
                    if (MessageBox.Show($"=============================\nCode: {lv_partnerList.SelectedItems[0].SubItems[0].Text}\nName: {lv_partnerList.SelectedItems[0].SubItems[1].Text}\n=============================\n삭제하시겠습니까?", "질문", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        MyListViewItem myListViewItem = (MyListViewItem)lv_partnerList.SelectedItems[0];
                        DBControl.Instance.Delete(myListViewItem.CodeModel);
                        ReflashList();
                        ClearTextBox();
                    }
                }
                else
                {
                    if (MessageBox.Show("미사용보기 상태에서만 삭제가 가능합니다.\n미사용보기로 변경하시겠습니까?",
                        "질문", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        SetPartnerList(MyCommon.FLAG.NONUSING);
                    };
                }
            }
        }

        private void UpdateOrAddPartner()
        {
            string code = tb_partnerCode.Text;
            string name = tb_partnerName.Text;
            string phone = tb_parterPhoneNumber.Text;
            string memo = tb_partnerMemo.Text;

            Partner partner = new Partner(code, name, phone, memo);
            if (MemoryControl.CompareCode(partner))
                UpdatePartner(partner);
            else
                AddPartner(partner);
        }

        private void AddPartner(Partner partner)
        {
            lv_partnerList.Items.Add(new MyListViewItem(partner));
            DBControl.Instance.Add(partner);
            ClearTextBox();
        }

        private void ClearTextBox()
        {
            tb_partnerCode.Enabled = true;
            tb_partnerCode.Text = string.Empty;
            tb_partnerName.Text = string.Empty;
            tb_parterPhoneNumber.Text = string.Empty;
            tb_partnerMemo.Text = string.Empty;
            tb_partnerCode.Focus();
        }

        private void UpdatePartner(Partner partner)
        {
            DBControl.Instance.Update(partner);
            ReflashList();
        }

        private void ReflashList()
        {
            SetPartnerList(NowFlag);
        }

        private void SetPartnerList(MyCommon.FLAG? flag)
        {
            lv_partnerList.Items.Clear();
            var partners = MemoryControl.GetPartnersToList();
            foreach(Partner partner in partners)
            {
                if (partner.FLAG == flag)
                    lv_partnerList.Items.Add(new MyListViewItem(partner));
                else if (flag == MyCommon.FLAG.ALL)
                {
                    MyListViewItem lvi = new MyListViewItem(partner);
                    if (partner.FLAG == MyCommon.FLAG.NONUSING)
                        lvi.BackColor = Color.Yellow;

                    lv_partnerList.Items.Add(lvi);
                }

            }
            NowFlag = flag;
        }

        private void lv_partnerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lv_partnerList.SelectedItems.Count != 0)
            {
                MyListViewItem myListViewItem = (MyListViewItem)lv_partnerList.SelectedItems[0];
                Partner partner = MemoryControl.GetModelFromCode(myListViewItem.CodeModel) as Partner;
                SetTextBoxFromPartner(partner);
            }
        }

        private void SetTextBoxFromPartner(Partner partner)
        {
            tb_partnerCode.Enabled = false;
            tb_partnerCode.Text = partner.CODE;
            tb_partnerName.Text = partner.NAME;
            tb_parterPhoneNumber.Text = partner.PHONE;
            tb_partnerMemo.Text = partner.MEMO;
            tb_partnerName.Focus();
        }

        private void PartnerForm_Load(object sender, EventArgs e)
        {
            SetPartnerList(MyCommon.FLAG.USING);
        }

        private void 전체ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SetPartnerList(MyCommon.FLAG.ALL);
            this.Text = "파트너 - 전체보기";
        }

        private void 사용ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SetPartnerList(MyCommon.FLAG.USING);
            this.Text = "파트너 - 사용보기";
        }

        private void 미사용ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            SetPartnerList(MyCommon.FLAG.NONUSING);
            this.Text = "파트너 - 미사용보기";
        }

        private void 사용ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MyListViewItem myListViewItem = (MyListViewItem)lv_partnerList.SelectedItems[0];
            DBControl.Instance.UpdateUsing(myListViewItem.CodeModel);
            ReflashList();
        }

        private void 미사용ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MyListViewItem myListViewItem = (MyListViewItem)lv_partnerList.SelectedItems[0];
            DBControl.Instance.UpdateUsing(myListViewItem.CodeModel);
            ReflashList();
        }

        private void 새로고침F5ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ClearTextBox();
        }

        private void tb_partnerCode_Leave(object sender, EventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox.Text != string.Empty)
            {
                CodeModel codeModel = new Partner();
                codeModel.CODE = textBox.Text;
                if (MemoryControl.CompareCode(codeModel))
                {
                    MessageBox.Show("코드 중복");
                    textBox.Focus();
                }
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            UpdateOrAddPartner();
        }
    }
}
