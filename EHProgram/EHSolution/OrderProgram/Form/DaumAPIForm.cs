﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrderProgram
{
    public partial class DaumAPIForm : Form
    {
        public delegate void FormSendDataHandler(string address);
        public event FormSendDataHandler FormSendEvent;
        private string address = string.Empty;
        public DaumAPIForm()
        {
            InitializeComponent();
        }

        private void DaumAPIForm_Load(object sender, EventArgs e)
        {
            this.Size = new Size(0, 0);
            Timer timer = new Timer();
            timer.Tick += Timer_Tick;
            timer.Interval = 100;
            timer.Start();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (webBrowser1.Document != null)
            {
                HtmlDocument htmlDocument = webBrowser1.Document;
                address = htmlDocument.GetElementById("sample5_address").GetAttribute("value");
                if (address != string.Empty)
                {
                    ((Timer)sender).Stop();
                    Close();
                }
            }
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            webBrowser1.Document.InvokeScript("sample5_execDaumPostcode");
        }

        private void DaumAPIForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Dispose();
            this.FormSendEvent(address);
        }
    }
}
