﻿using OrderProgram.Model;
using OrderProgram.MyControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrderProgram
{
    public partial class SelectForm : Form
    {
        public delegate void SelectCallBack(CodeModel codeModel);
        public event SelectCallBack SelectItem;
        public string fillter = string.Empty;
        CodeModel CodeModel = null;
        public SelectForm(CodeModel codeModel)
        {
            this.CodeModel = codeModel;
            InitializeComponent();
            
        }

        private void SelectForm_Load(object sender, EventArgs e)
        {
            List<CodeModel> codeModels = new List<CodeModel>();
            if (this.CodeModel is Partner)
                codeModels.AddRange(MemoryControl.GetPartnersToList());
            else
                codeModels.AddRange(MemoryControl.GetHospitals());

            foreach (CodeModel codeModel in codeModels)
            {
                if (codeModel.FLAG == Common.MyCommon.FLAG.USING)
                {
                    if (fillter == string.Empty)
                    {
                        MyListViewItem myListViewItem = new MyListViewItem(codeModel);
                        lv_codemodel.Items.Add(myListViewItem);
                    }
                    else
                    {
                        if (codeModel.ToString().Contains(fillter))
                        {
                            MyListViewItem myListViewItem = new MyListViewItem(codeModel);
                            lv_codemodel.Items.Add(myListViewItem);
                        }
                    }
                }
            }

            if(lv_codemodel.Items.Count == 0)
            {
                this.DialogResult = DialogResult.Retry;
                Close();
                return;
            }

            lv_codemodel.Items[0].Selected = true;
        }

        private void lv_codemodel_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            SelectCallBackBaseForm();
            Close();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Enter:
                    {
                        SelectCallBackBaseForm();
                        return true;
                    }
                default:
                    return base.ProcessCmdKey(ref msg, keyData);
            }
        
        }

        private void SelectCallBackBaseForm()
        {
            this.DialogResult = DialogResult.OK;
            MyListViewItem myListViewItem = (MyListViewItem)lv_codemodel.SelectedItems[0];
            SelectItem(myListViewItem.CodeModel);
        }
    }
}
