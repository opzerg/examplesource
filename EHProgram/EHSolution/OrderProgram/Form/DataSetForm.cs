﻿using OrderProgram.Common;
using OrderProgram.Model;
using OrderProgram.MyControl;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace OrderProgram
{
    public partial class DataSetForm : Form
    {
        private static TreeNode SelectTreeNode = null;

        public DataSetForm()
        {
            InitializeComponent();
            InitializeControl();
        }

        private void InitializeControl()
        {
            #region ContextMenuStrip
            ContextMenuStrip contextMenuStrip1 = new ContextMenuStrip();
            contextMenuStrip1.Items.Add("대분류 삭제");
            contextMenuStrip1.Name = "cms_large";
            contextMenuStrip1.Items[0].Click += ContextMenuItem_Click;

            lb_large.ContextMenuStrip = contextMenuStrip1;

            ContextMenuStrip contextMenuStrip2 = new ContextMenuStrip();
            contextMenuStrip2.Items.Add("소분류 삭제");
            contextMenuStrip2.Name = "cms_small";
            contextMenuStrip2.Items[0].Click += ContextMenuItem_Click;

            lb_small.ContextMenuStrip = contextMenuStrip2;


            ContextMenuStrip contextMenuStrip3 = new ContextMenuStrip();
            contextMenuStrip3.Items.Add("삭제");
            contextMenuStrip3.Name = "cms_tree";
            contextMenuStrip3.Items[0].Click += ContextMenuItem_Click;

            treeView1.ContextMenuStrip = contextMenuStrip3;
            #endregion

        }
        
        private void ContextMenuItem_Click(object sender, EventArgs e)
        {
            #region ContextMenuStrip Click
            ToolStripMenuItem toolStripMenuItem = sender as ToolStripMenuItem;
            if(toolStripMenuItem.Text.Contains("대분류"))
            {
                if(lb_large.SelectedItem != null)
                {
                    Category category = lb_large.SelectedItem as Category;
                    if (!DeleteMessageBox(category))
                        return;
                    DBControl.Instance.Delete(category);
                    lb_large.Items.Remove(category);
                }
            }else if(toolStripMenuItem.Text.Contains("소분류"))
            {
                if (lb_small.SelectedItem != null)
                {
                    Category category = lb_small.SelectedItem as Category;
                    if (!DeleteMessageBox(category))
                        return;
                    DBControl.Instance.Delete(category);
                    lb_small.Items.Remove(category);
                }
            }else if(toolStripMenuItem.Text.Equals("삭제"))
            {
                if(treeView1.SelectedNode != null)
                {
                    Category category = treeView1.SelectedNode.Tag as Category;
                    if (!DeleteMessageBox(category))
                        return;
                    DBControl.Instance.DeleteTreeCategory(SelectTreeNode.Name, category.CODE);
                    treeView1.Nodes.Remove(treeView1.SelectedNode);
                }
            }
            #endregion
        }

        private bool DeleteMessageBox(Category category) =>
            MessageBox.Show($"{category.NAME}을 삭제하시겠습니까?","질문", MessageBoxButtons.YesNo) == DialogResult.Yes;

        private void DataSetForm_Load(object sender, EventArgs e)
        {
            var 대분류 = MemoryControl.GetCategories(MyCommon.CATEGORY_KIND.대분류);
            lb_large.Items.AddRange(대분류.ToArray());

            var 소분류 = MemoryControl.GetCategories(MyCommon.CATEGORY_KIND.소분류);
            lb_small.Items.AddRange(소분류.ToArray());

            CreateTreeView(대분류);
            
        }

        private void CreateTreeView(List<Category> 대분류)
        {
            foreach (Category category in 대분류)
            {
                TreeNode treeNode = TreeRootAdd(category);
                var 자식노드 = DBControl.Instance.GetCategoryTree(category.CODE);
                TreeChildAdd(treeNode, 자식노드);
            }
        }

        private void tb_large_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar is (char)Keys.Enter)
            {
                TextBox textBox = sender as TextBox;
                int code = 0;
                string name = textBox.Text;
                Category category = null;
                if (textBox.Name.Contains("large"))
                {
                    code = DBControl.Instance.LargeCategoryCode++;
                    category = CreateCategory(code.ToString(), name, MyCommon.CATEGORY_KIND.대분류);
                    lb_large.Items.Add(category);
                    TreeRootAdd(category);
                }
                else
                {
                    code = DBControl.Instance.SmallCategoryCode++;
                    category = CreateCategory(code.ToString(), name, MyCommon.CATEGORY_KIND.소분류);
                    lb_small.Items.Add(category);
                }

                DBControl.Instance.Add(category);
                textBox.Text = "";
            }
        }

        private TreeNode TreeRootAdd(Category category)
        {
            TreeNode node = new TreeNode();
            node.Text = category.NAME;
            node.Tag = category;
            node.Name = category.CODE;
            node.ImageIndex = 0;
            node.SelectedImageIndex = 0;

            treeView1.Nodes.Add(node);
            return node;
        }

        private void TreeChildAdd(TreeNode parent, Category category)
        {
            foreach(TreeNode treeNode in parent.Nodes)
            {
                if(treeNode.Name == category.CODE)
                {
                    MessageBox.Show("이미 존재합니다!!");
                    return;
                }
            }
            TreeNode node = new TreeNode();
            node.Text = category.NAME;
            node.Tag = category;
            node.Name = category.CODE;
            node.ImageIndex = 0;
            node.SelectedImageIndex = 0;

            parent.Nodes.Add(node);
        }

        private void TreeChildAdd(TreeNode parent, List<Category> categories)
        {
            foreach (Category category in categories)
            {
                TreeNode node = new TreeNode();
                node.Text = category.NAME;
                node.Tag = category;
                node.Name = category.CODE;
                node.ImageIndex = 0;
                node.SelectedImageIndex = 0;

                parent.Nodes.Add(node);
            }
        }

        private Category CreateCategory(string code, string name, MyCommon.CATEGORY_KIND kind) => 
            new Category(code, name, kind);

        private void treeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode treeNode = e.Node;
            while(true)
            {
                if (treeNode.Parent == null)
                    break;
                treeNode = treeNode.Parent;
            }
            SelectTreeNode = treeNode;
        }

        private void lb_small_DoubleClick(object sender, EventArgs e)
        {
            if (lb_small.SelectedItem != null)
            {
                Category category = lb_small.SelectedItem as Category;
                if (MessageBox.Show($"{SelectTreeNode.Text}에 {category.NAME}을 추가 하시겠습니까?","질문", MessageBoxButtons.YesNo)
                    == DialogResult.Yes)
                {
                    TreeChildAdd(SelectTreeNode, category);
                    DBControl.Instance.AddTreeCategory(SelectTreeNode.Tag as Category, category);
                }
            }
            
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if(keyData == Keys.Escape)
            {
                Close();
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
