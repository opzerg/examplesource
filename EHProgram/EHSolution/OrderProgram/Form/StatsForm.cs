﻿using OrderProgram.Common;
using OrderProgram.Model;
using OrderProgram.MyControl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static OrderProgram.Model.HospitalOrder;

namespace OrderProgram
{
    public partial class StatsForm : Form
    {

        private static class MyColumnsHeader
        {
            public static ColumnHeader Number
            {
                get
                {
                    ColumnHeader columnHeader = new ColumnHeader();
                    columnHeader.Text = "순번";
                    return columnHeader;
                }
            }

            public static ColumnHeader Box
            {
                get
                {
                    ColumnHeader columnHeader = new ColumnHeader();
                    columnHeader.Text = "박스(EA)";
                    columnHeader.Width = 80;
                    return columnHeader;
                }
            }

            public static ColumnHeader Price
            {
                get
                {
                    ColumnHeader columnHeader = new ColumnHeader();
                    columnHeader.Text = "금액(원)";
                    columnHeader.Width = 150;
                    return columnHeader;
                }
            }

            public static ColumnHeader Surtax
            {
                get
                {
                    ColumnHeader columnHeader = new ColumnHeader();
                    columnHeader.Text = "부가세(원)";
                    columnHeader.Width = 150;
                    return columnHeader;
                }
            }

            public static ColumnHeader Hospital
            {
                get
                {
                    ColumnHeader columnHeader = new ColumnHeader();
                    columnHeader.Text = "병원명";
                    columnHeader.Width = 180;
                    return columnHeader;
                }
            }

            public static ColumnHeader Partner
            {
                get
                {
                    ColumnHeader columnHeader = new ColumnHeader();
                    columnHeader.Text = "요청자";
                    return columnHeader;
                }
            }

            public static ColumnHeader OrderKind
            {
                get
                {
                    ColumnHeader columnHeader = new ColumnHeader();
                    columnHeader.Text = "종류";
                    columnHeader.Width = 180;
                    return columnHeader;
                }
            }

            public static ColumnHeader[] GetColumns()
            {
                return new ColumnHeader[] { MyColumnsHeader.Number, MyColumnsHeader.Box, MyColumnsHeader.Price,
                MyColumnsHeader.Surtax, MyColumnsHeader.Hospital, MyColumnsHeader.Partner, MyColumnsHeader.OrderKind};
            }

        }

        private DateTime Pre_Day = DateTime.Now;
        private DateTime From_Day = DateTime.Now;
        private List<MyCommon.ORDER_KIND> OrderKinds = 
            new List<MyCommon.ORDER_KIND>();
        public StatsForm(DateTime pre, DateTime from)
        {
            Pre_Day = pre;
            From_Day = from;
            InitializeComponent();
            this.Text = $"통계 날짜: {pre:yyyy/MM/dd}~{from:yyyy/MM/dd}";
        }

        private void StatsForm_Load(object sender, EventArgs e)
        {
            cb_hospital.Items.Add("전 체");
            cb_hospital.Items.AddRange(MemoryControl.GetHospitals().ToArray());
            cb_hospital.SelectedIndex = 0;
        }

        private void 분류세팅(MyCommon.CATEGORY_KIND 분류)
        {
            cb_class.Items.Clear();
            cb_class.Items.AddRange(MemoryControl.GetCategories(분류).ToArray());
        }
        
        private void rb_large_Click(object sender, EventArgs e)
        {
            분류세팅(MyCommon.CATEGORY_KIND.대분류);
        }

        private void rb_small_Click(object sender, EventArgs e)
        {
            분류세팅(MyCommon.CATEGORY_KIND.소분류);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox kindAll = sender as CheckBox;
            cb_무상.Checked = kindAll.Checked;
            cb_서비스.Checked = kindAll.Checked;
            cb_유상.Checked = kindAll.Checked;
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            if (cb_class.SelectedItem == null)
            {
                MessageBox.Show("분류를 선택해 주세요.");
                return;
            }

            if (OrderKinds.Count == 0)
            {
                MessageBox.Show("종류를 선택해 주세요.");
                return;
            }

            Hospital hospital = cb_hospital.SelectedItem as Hospital;
            Category category = cb_class.SelectedItem as Category;

            //순번, 박스, 금액, 부가세, 병원, 파트너, 종류
            listView1.Columns.Clear();
            listView1.Columns.AddRange(MyColumnsHeader.GetColumns());
            listView1.Items.Clear();
            
            //병원선택 안함
            if (hospital is null)
            {
                int index = 0;
                foreach (HospitalOrder hospitalOrder in MemoryControl.GetHospitalOrders())
                {
                    foreach (Order order in hospitalOrder.Orders)
                    {
                        string category_code = string.Empty;
                        if (category.KIND == MyCommon.CATEGORY_KIND.대분류)
                            category_code = order.LargeCategory.CODE;
                        else
                            category_code = order.SmallCategory.CODE;

                        if (category_code == category.CODE)
                        {
                            string items = string.Empty;
                            if (OrderKinds.Count != 3)
                            {
                                if (OrderKinds.Contains(order.OrderKind))
                                {
                                    items = $"{++index}@{order.Box:n0}@{order.Price:n0}@{order.Surtax:n0}@{hospitalOrder.Hospital.NAME}@" +
                                    $"{hospitalOrder.Partner.NAME}@{order.OrderKind.ToString()}";
                                }
                            }
                            else
                            {
                                items = $"{++index}@{order.Box:n0}@{order.Price:n0}@{order.Surtax:n0}@{hospitalOrder.Hospital.NAME}@" +
                                    $"{hospitalOrder.Partner.NAME}@{order.OrderKind.ToString()}";
                            }

                            if (items != string.Empty)
                            {
                                ListViewItem lvi = new ListViewItem(items.Split('@'));
                                listView1.Items.Add(lvi);
                            }
                        }

                    }
                }
            }
            //병원선택
            else
            {
                List<HospitalOrder> HopsitalOrders = null;
                if (MessageBox.Show($"{Pre_Day:yyyy/MM/dd}~{From_Day:yyyy/MM/dd} 기간만큼만 조회하시겠습니까?", "질문", MessageBoxButtons.YesNo)
                    == DialogResult.Yes)
                    HopsitalOrders = MemoryControl.GetHospitalOrders();
                else
                    HopsitalOrders = DBControl.Instance.GetAllHospitalOrders(hospital.CODE);

                int index = 0;
                foreach (HospitalOrder hospitalOrder in HopsitalOrders)
                {
                    if (hospitalOrder.Hospital.CODE == hospital.CODE)
                    {
                        foreach (Order order in hospitalOrder.Orders)
                        {
                            string category_code = string.Empty;
                            if (category.KIND == MyCommon.CATEGORY_KIND.대분류)
                                category_code = order.LargeCategory.CODE;
                            else
                                category_code = order.SmallCategory.CODE;

                            if (category_code == category.CODE)
                            {
                                string items = string.Empty;
                                if (OrderKinds.Count != 3)
                                {
                                    if (OrderKinds.Contains(order.OrderKind))
                                    {
                                        items = $"{++index}@{order.Box:n0}@{order.Price:n0}@{order.Surtax:n0}@{hospitalOrder.Hospital.NAME}@" +
                                                   $"{hospitalOrder.Partner.NAME}@{order.OrderKind.ToString()}";
                                    }
                                }
                                else
                                {
                                    items = $"{++index}@{order.Box:n0}@{order.Price:n0}@{order.Surtax:n0}@{hospitalOrder.Hospital.NAME}@" +
                                                $"{hospitalOrder.Partner.NAME}@{order.OrderKind.ToString()}";
                                }

                                if (items != string.Empty)
                                {
                                    ListViewItem lvi = new ListViewItem(items.Split('@'));
                                    listView1.Items.Add(lvi);
                                }
                            }

                        }
                    }
                }
            }


            int boxIndex = 0;
            int priceIndex = 0;
            int surtaxIndex = 0;
            int kindIndex = 0;
            //컬럼헤더 가져오기
            int idx = 0;
            foreach(ColumnHeader ch in listView1.Columns)
            {
                if (ch.Text.Contains("박스"))
                    boxIndex = idx;
                else if (ch.Text.Contains("금액"))
                    priceIndex = idx;
                else if (ch.Text.Contains("부가세"))
                    surtaxIndex = idx;
                else if (ch.Text.Contains("종류"))
                    kindIndex = idx;
                idx++;
            }

            int totalBox = 0;
            long totalPrice = 0;
            long totalSurtax = 0;
            string strOrderKind = string.Empty;
            //총합
            foreach(MyCommon.ORDER_KIND orderKind in OrderKinds)
            {
                int box = 0;
                long price = 0;
                long surtax = 0;
                foreach (ListViewItem lvi in listView1.Items)
                {
                    if(lvi.SubItems[kindIndex].Text == orderKind.ToString())
                    {
                        box += int.Parse(lvi.SubItems[boxIndex].Text.Replace(",", ""));
                        price += long.Parse(lvi.SubItems[priceIndex].Text.Replace(",",""));
                        surtax += long.Parse(lvi.SubItems[surtaxIndex].Text.Replace(",",""));
                    }
                }
                totalBox += box;
                totalPrice += price;
                totalSurtax += totalSurtax;
                strOrderKind += $"{orderKind} ";
                string[] items = new string[7] {$"{orderKind}", $"{box:n0}",$"{price:n0}",$"{surtax:n0}","","",$"" };
                ListViewItem listViewItem = new ListViewItem(items);
                listViewItem.BackColor = Color.LightSalmon;
                listView1.Items.Add(listViewItem);
            }

            string[] totalItems = new string[7] { "종합", $"{totalBox:n0}", $"{totalPrice:n0}", $"{totalSurtax:n0}", "", "", $"{strOrderKind}" };
            ListViewItem totalListViewItems = new ListViewItem(totalItems);
            totalListViewItems.BackColor = Color.LightYellow;
            listView1.Items.Add(totalListViewItems);

        }

        private void cb_유상_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            OrderKindsManagement(checkBox.Checked, MyCommon.ORDER_KIND.유상);
        }

        private void OrderKindsManagement(bool check, MyCommon.ORDER_KIND 종류)
        {
            if (check)
                OrderKinds.Add(종류);
            else
                OrderKinds.Remove(종류);
        }

        private void cb_무상_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            OrderKindsManagement(checkBox.Checked, MyCommon.ORDER_KIND.무상);
        }

        private void cb_서비스_CheckedChanged_1(object sender, EventArgs e)
        {
            CheckBox checkBox = sender as CheckBox;
            OrderKindsManagement(checkBox.Checked, MyCommon.ORDER_KIND.서비스);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
                Close();
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
