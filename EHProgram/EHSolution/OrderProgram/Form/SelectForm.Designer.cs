﻿namespace OrderProgram
{
    partial class SelectForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lv_codemodel = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // lv_codemodel
            // 
            this.lv_codemodel.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.lv_codemodel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv_codemodel.FullRowSelect = true;
            this.lv_codemodel.Location = new System.Drawing.Point(0, 0);
            this.lv_codemodel.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.lv_codemodel.MultiSelect = false;
            this.lv_codemodel.Name = "lv_codemodel";
            this.lv_codemodel.Size = new System.Drawing.Size(252, 383);
            this.lv_codemodel.TabIndex = 0;
            this.lv_codemodel.UseCompatibleStateImageBehavior = false;
            this.lv_codemodel.View = System.Windows.Forms.View.Details;
            this.lv_codemodel.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lv_codemodel_MouseDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "코드";
            this.columnHeader1.Width = 78;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "이름";
            this.columnHeader2.Width = 164;
            // 
            // SelectForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(22F, 37F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(252, 383);
            this.Controls.Add(this.lv_codemodel);
            this.Font = new System.Drawing.Font("굴림", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "SelectForm";
            this.Text = "SelectForm";
            this.Load += new System.EventHandler(this.SelectForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView lv_codemodel;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
    }
}