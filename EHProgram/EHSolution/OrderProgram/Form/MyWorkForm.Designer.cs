﻿namespace OrderProgram
{
    partial class MyWorkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.btn_partnerSearch = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_partner = new System.Windows.Forms.TextBox();
            this.lb_memo1 = new System.Windows.Forms.Label();
            this.tb_memo1 = new System.Windows.Forms.TextBox();
            this.btn_add = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.메뉴ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.메모추가ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.메모삭제ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(11, 23);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(266, 51);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // btn_partnerSearch
            // 
            this.btn_partnerSearch.Location = new System.Drawing.Point(594, 23);
            this.btn_partnerSearch.Name = "btn_partnerSearch";
            this.btn_partnerSearch.Size = new System.Drawing.Size(35, 29);
            this.btn_partnerSearch.TabIndex = 7;
            this.btn_partnerSearch.Text = "button1";
            this.btn_partnerSearch.UseVisualStyleBackColor = true;
            this.btn_partnerSearch.Click += new System.EventHandler(this.btn_partnerSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(360, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 38);
            this.label2.TabIndex = 5;
            this.label2.Text = "요청자";
            // 
            // tb_partner
            // 
            this.tb_partner.Location = new System.Drawing.Point(432, 23);
            this.tb_partner.Name = "tb_partner";
            this.tb_partner.Size = new System.Drawing.Size(157, 51);
            this.tb_partner.TabIndex = 1;
            // 
            // lb_memo1
            // 
            this.lb_memo1.AutoSize = true;
            this.lb_memo1.Location = new System.Drawing.Point(8, 77);
            this.lb_memo1.Name = "lb_memo1";
            this.lb_memo1.Size = new System.Drawing.Size(93, 38);
            this.lb_memo1.TabIndex = 8;
            this.lb_memo1.Text = "메모";
            // 
            // tb_memo1
            // 
            this.tb_memo1.Location = new System.Drawing.Point(61, 74);
            this.tb_memo1.Multiline = true;
            this.tb_memo1.Name = "tb_memo1";
            this.tb_memo1.Size = new System.Drawing.Size(568, 64);
            this.tb_memo1.TabIndex = 2;
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(635, 23);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(82, 29);
            this.btn_add.TabIndex = 13;
            this.btn_add.Text = "저장";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.메뉴ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(715, 40);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 메뉴ToolStripMenuItem
            // 
            this.메뉴ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.메모추가ToolStripMenuItem,
            this.메모삭제ToolStripMenuItem});
            this.메뉴ToolStripMenuItem.Name = "메뉴ToolStripMenuItem";
            this.메뉴ToolStripMenuItem.Size = new System.Drawing.Size(75, 36);
            this.메뉴ToolStripMenuItem.Text = "메뉴";
            // 
            // 메모추가ToolStripMenuItem
            // 
            this.메모추가ToolStripMenuItem.Name = "메모추가ToolStripMenuItem";
            this.메모추가ToolStripMenuItem.Size = new System.Drawing.Size(210, 38);
            this.메모추가ToolStripMenuItem.Text = "메모추가";
            this.메모추가ToolStripMenuItem.Click += new System.EventHandler(this.메모추가ToolStripMenuItem_Click);
            // 
            // 메모삭제ToolStripMenuItem
            // 
            this.메모삭제ToolStripMenuItem.Name = "메모삭제ToolStripMenuItem";
            this.메모삭제ToolStripMenuItem.Size = new System.Drawing.Size(210, 38);
            this.메모삭제ToolStripMenuItem.Text = "메모삭제";
            this.메모삭제ToolStripMenuItem.Click += new System.EventHandler(this.메모삭제ToolStripMenuItem_Click);
            // 
            // MyWorkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(22F, 38F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 145);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.tb_memo1);
            this.Controls.Add(this.lb_memo1);
            this.Controls.Add(this.btn_partnerSearch);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_partner);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("굴림", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "MyWorkForm";
            this.Text = "내업무 - 추가";
            this.Load += new System.EventHandler(this.MyWorkForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button btn_partnerSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_partner;
        private System.Windows.Forms.Label lb_memo1;
        private System.Windows.Forms.TextBox tb_memo1;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 메뉴ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 메모추가ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 메모삭제ToolStripMenuItem;
    }
}