﻿using OrderProgram.Common;
using OrderProgram.Model;
using OrderProgram.MyControl;
using System;
using System.Collections;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace OrderProgram
{
    public partial class MainForm : Form
    {
        public class SelectModel
        {
            public CodeModel CodeModel
            {
                get;set;
            }
            public int SelectIndex
            {
                get;set;
            }
            public SelectModel(CodeModel codeModel, int selectIndex)
            {
                this.CodeModel = codeModel;
                this.SelectIndex = selectIndex;
            }
            public void Deconstruct(out CodeModel CodeModel, out int SelectIndex)
            {
                CodeModel = this.CodeModel;
                SelectIndex = this.SelectIndex;
            }
        }

        private static SelectModel SelectCodeModel = null;
        public MainForm()
        {
            InitializeComponent();
        }

        private void 병원추가ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowHospitalForm();
        }

        private void ShowHospitalForm()
        {
            HospitalForm hospitalForm = new HospitalForm();
            hospitalForm.StartPosition = FormStartPosition.CenterScreen;
            hospitalForm.Show();
        }

        private void 요청자ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowPartnerForm();
        }

        private void ShowPartnerForm()
        {
            PartnerForm partnerForm = new PartnerForm();
            partnerForm.StartPosition = FormStartPosition.CenterScreen;
            partnerForm.Show();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            #region //단축키
            Keys key = keyData & ~(Keys.Shift | Keys.Control);
            switch(key)
            {
                case Keys.Enter:
                    {
                        //추가 단축키
                        if ((keyData & Keys.Shift) != 0)
                        {
                            AddItem();
                        }
                        return true;
                    }
                case Keys.Tab:
                    {
                        //탭 페이지 넘기기
                        ChangeTabPage();
                        return true;
                    }
                case Keys.H:
                    {
                        //병원
                        if ((keyData & Keys.Shift) != 0)
                            ShowHospitalForm();
                        return true;
                    }
                case Keys.P:
                    {
                        if ((keyData & Keys.Shift) != 0)
                            ShowPartnerForm();
                        return true;
                    }
                case Keys.D:
                    {
                        if ((keyData & Keys.Shift) != 0)
                            ShowDataSetForm();
                        return true;
                    }
                case Keys.D1: case Keys.D2: case Keys.D3: case Keys.D4: case Keys.D5:
                    {
                        if((keyData & Keys.Control) != 0)
                            SelectKeyState(key);
                        return true;
                    }
                case Keys.F5:
                    {
                        ReflashForm();
                        return true;
                    }
                case Keys.F:
                    {
                        if ((keyData & Keys.Control) != 0)
                        {
                            FindFormShow();
                        }
                        return true;
                    }
                case Keys.S:
                    {
                        if((keyData & Keys.Shift) != 0)
                        {
                            ShowStatsForm();
                        }
                        return true;
                    }
                default:
                    return base.ProcessCmdKey(ref msg, keyData);
            }
            #endregion
        }

        private void ShowDataSetForm()
        {
            DataSetForm dataSetForm = new DataSetForm();
            dataSetForm.StartPosition = FormStartPosition.CenterScreen;
            dataSetForm.ShowDialog();
        }

        private void ReflashForm()
        {
            dtp_pre.Value = DateTime.Now;
            dtp_from.Value = DateTime.Now;
            rb_progress.Checked = true;
            cb_deleteView.Checked = false;
            tc_main.SelectedIndex = 0;
            SearchOrderAndMyWork(dtp_pre.Value, dtp_from.Value);
        }

        private void SelectKeyState(Keys key)
        {
            if (key == Keys.D1)
            {
                MyWorkListLoad(MyCommon.STATUS.완료);
                OrderListLoad(MyCommon.STATUS.완료);
                rb_done.Checked = true;
            }
            else if (key == Keys.D2)
            {
                MyWorkListLoad(MyCommon.STATUS.보류);
                OrderListLoad(MyCommon.STATUS.보류);
                rb_hold.Checked = true;
            }
            else if (key == Keys.D3)
            {
                MyWorkListLoad(MyCommon.STATUS.진행);
                OrderListLoad(MyCommon.STATUS.진행);
                rb_progress.Checked = true;
            }
            else if (key == Keys.D4)
            {
                MyWorkListLoad(MyCommon.STATUS.전체);
                OrderListLoad(MyCommon.STATUS.전체);
                rb_all.Checked = true;
            }
            else if (key == Keys.D5)
            {
                MyWorkListLoad(MyCommon.STATUS.취소);
                OrderListLoad(MyCommon.STATUS.취소);
                rb_cancel.Checked = true;
            }
        }

        private void AddItem()
        {
            //발주 현황 추가
            if (tc_main.SelectedIndex == 0)
                ShowOrderForm();
            //내 업무 추가
            else if (tc_main.SelectedIndex == 1)
                ShowMyWorkForm();
        }

        private void ChangeTabPage()
        {
            //발주 현황
            if (tc_main.SelectedIndex == 0)
                tc_main.SelectedIndex = 1;
            //내 업무
            else if (tc_main.SelectedIndex == 1)
                tc_main.SelectedIndex = 0;
        }

        private void ShowMyWorkForm()
        {
            MyWorkForm myWorkForm = new MyWorkForm();
            myWorkForm.MyWorkAddCallBack += MyWorkForm_MyWorkCallBack;
            myWorkForm.StartPosition = FormStartPosition.CenterScreen;
            myWorkForm.Show();
        }

        private void MyWorkForm_MyWorkCallBack(MyWork myWork)
        {
            MyListViewItem myListViewItem = new MyListViewItem(myWork);
            lv_myWork.Items.Add(myListViewItem);
        }

        private void ShowOrderForm()
        {
            OrderForm orderForm = new OrderForm();
            orderForm.StartPosition = FormStartPosition.CenterScreen;
            orderForm.AddCallback += OrderForm_AddCallback;
            orderForm.Show();
        }

        private void OrderForm_AddCallback(HospitalOrder hospitalOrder)
        {
            if (rb_progress.Checked | rb_all.Checked)
            {
                string[] str = hospitalOrder.ToString().Split(',');
                str[2] = $"{int.Parse(str[2]):n0}";
                str[3] = $"{int.Parse(str[3]):n0}";
                MyListViewItem myListViewItem = new MyListViewItem(str);
                myListViewItem.CodeModel = hospitalOrder;
                lv_order.Items.Add(myListViewItem);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            DBControl.Instance.Init();
            SearchOrderAndMyWork(dtp_pre.Value, dtp_from.Value);
        }

        private void MyWorkListLoad(MyCommon.STATUS status, MyCommon.FLAG flag = MyCommon.FLAG.USING)
        {
            lv_myWork.Items.Clear();
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += new DoWorkEventHandler(BackgroundWorker_DoWork);
            if (flag == MyCommon.FLAG.USING)
                backgroundWorker.RunWorkerAsync(status);
            else
                backgroundWorker.RunWorkerAsync(flag);
        }

        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            MyCommon.STATUS? status = null;
            MyCommon.FLAG? flag = null;
            if (e.Argument is MyCommon.STATUS)
                status = (MyCommon.STATUS)e.Argument;
            else if (e.Argument is MyCommon.FLAG)
                flag = (MyCommon.FLAG)e.Argument;

            foreach (MyWork myWork in MemoryControl.GetMyWorks())
            {
                if (status is null)
                {
                    if (myWork.FLAG == MyCommon.FLAG.NONUSING)
                    {
                        lv_myWork.Invoke(new Action(() =>
                        {
                            MyListViewItem myListViewItem = new MyListViewItem(myWork);
                            lv_myWork.Items.Add(myListViewItem);
                        }));
                    }
                }
                else if (status == MyCommon.STATUS.전체 && 
                    myWork.Status != MyCommon.STATUS.취소)
                {
                    lv_myWork.Invoke(new Action(() =>
                    {
                        MyListViewItem myListViewItem = new MyListViewItem(myWork);
                        if (myWork.FLAG == MyCommon.FLAG.USING)
                        {
                            myListViewItem.BackColor = MyCommon.GetBackColorFromStatus(myWork.Status);
                        } else if(myWork.FLAG == MyCommon.FLAG.NONUSING)
                        {
                            if (!cb_deleteView.Checked)
                                return;
                            (myListViewItem.SubItems[3].Text,
                            myListViewItem.BackColor) = MyCommon.GetBackColorFromNonUsing();
                        }
                        lv_myWork.Items.Add(myListViewItem);
                    }));
                }
                else if (myWork.Status == status && myWork.FLAG == MyCommon.FLAG.USING)
                {
                    lv_myWork.Invoke(new Action(() =>
                    {
                        MyListViewItem myListViewItem = new MyListViewItem(myWork);
                        lv_myWork.Items.Add(myListViewItem);
                    }));
                    
                }
            }
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            SearchOrderAndMyWork(dtp_pre.Value, dtp_from.Value);
            MyCommon.STATUS status = GetMainFormMyWorkStatus();
            MyWorkListLoad(status);
            OrderListLoad(status);
        }

        private void SearchOrderAndMyWork(DateTime pre, DateTime from)
        {
            this.Cursor = Cursors.WaitCursor;
            DBControl.Instance.SetWorks($"{pre:yyyy/MM/dd} 00:00:00",
                $"{from:yyyy/MM/dd} 23:59:59");
            DBControl.Instance.SetHospitalOrders($"{pre:yyyy/MM/dd} 00:00:00",
                $"{from:yyyy/MM/dd} 23:59:59");
            this.Cursor = Cursors.Default;
        }

        private void OrderListLoad(MyCommon.STATUS status)
        {
            lv_order.Items.Clear();
            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += BackgroundWorker_DoWorkTwo;
            backgroundWorker.RunWorkerAsync(status);
        }

        private void BackgroundWorker_DoWorkTwo(object sender, DoWorkEventArgs e)
        {
            MyCommon.STATUS status = (MyCommon.STATUS)e.Argument;
            foreach (HospitalOrder hospitalOrder in MemoryControl.GetHospitalOrders())
            {
                if (status == MyCommon.STATUS.전체 &&
                    hospitalOrder.Status != MyCommon.STATUS.취소)
                {
                    lv_order.Invoke(new Action(() =>
                    {
                        string[] str = hospitalOrder.ToString().Split(',');
                        str[2] = $"{int.Parse(str[2]):n0}";
                        str[3] = $"{int.Parse(str[3]):n0}";
                        MyListViewItem myListViewItem = new MyListViewItem(str);
                        myListViewItem.CodeModel = hospitalOrder;
                        myListViewItem.BackColor = MyCommon.GetBackColorFromStatus(hospitalOrder.Status);
                        lv_order.Items.Add(myListViewItem);
                    }));
                }
                else if (hospitalOrder.Status == status)
                {
                    lv_order.Invoke(new Action(() =>
                    {
                        string[] str = hospitalOrder.ToString().Split(',');
                        str[2] = $"{int.Parse(str[2]):n0}";
                        str[3] = $"{int.Parse(str[3]):n0}";
                        MyListViewItem myListViewItem = new MyListViewItem(str);
                        myListViewItem.CodeModel = hospitalOrder;
                        lv_order.Items.Add(myListViewItem);
                    }));

                }
            }
        }

        private MyCommon.STATUS GetMainFormMyWorkStatus()
        {
            foreach(Control control in gb_state.Controls)
            {
                if(control is RadioButton)
                {
                    RadioButton radioButton = control as RadioButton;
                    if(radioButton.Checked)
                    {
                        foreach(MyCommon.STATUS status in (MyCommon.STATUS[])Enum.GetValues(typeof(MyCommon.STATUS)))
                        {
                            if(radioButton.Text.Contains(status.ToString()))
                            {
                                return status;
                            }
                        }
                    }
                }
            }
            return MyCommon.STATUS.NONE;
        }

        private void btn_year_Click(object sender, EventArgs e)
        {
            ChangeYear();
        }

        private void ChangeYear()
        {
            dtp_pre.Value = dtp_from.Value.AddYears(-1);
        }

        private void btn_month_Click(object sender, EventArgs e)
        {
            ChangeMonth();
        }

        private void ChangeMonth()
        {
            dtp_pre.Value = dtp_from.Value.AddMonths(-1);
        }

        private void btn_week_Click(object sender, EventArgs e)
        {
            ChangeWeek();
        }

        private void ChangeWeek()
        {
            dtp_pre.Value = dtp_from.Value.AddDays(-7);
        }

        private void 완료ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CodeModelChangeStatusEvent(MyCommon.STATUS.완료);
        }

        private void CodeModelChangeStatusEvent(MyCommon.STATUS status)
        {
            if (SelectCodeModel != null)
            {
                if (MessageBox.Show($"날짜: {SelectCodeModel.CodeModel.RequestDate} 요청자: {SelectCodeModel.CodeModel.NAME}의 상태를\n" +
                    $"{SelectCodeModel.CodeModel.Status}에서 {status}로 바꾸시겠습니까?", "질문", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    SelectCodeModel.CodeModel.Status = status;
                    DBControl.Instance.UpdateStatus(SelectCodeModel.CodeModel);
                    CodeModelChangeStatus();
                }
               
            }
        }

        private void CodeModelChangeStatus()
        {
            if(SelectCodeModel.CodeModel is MyWork)
            {
                if (rb_all.Checked &&
                    SelectCodeModel.CodeModel.Status != MyCommon.STATUS.취소)
                {
                    lv_myWork.Items[SelectCodeModel.SelectIndex].SubItems[3].Text = 
                        SelectCodeModel.CodeModel.Status.ToString();
                    lv_myWork.Items[SelectCodeModel.SelectIndex].BackColor = 
                        MyCommon.GetBackColorFromStatus(SelectCodeModel.CodeModel.Status);
                }
                else
                    lv_myWork.Items.RemoveAt(SelectCodeModel.SelectIndex);
            } else if(SelectCodeModel.CodeModel is HospitalOrder)
            {
                if (rb_all.Checked && 
                    SelectCodeModel.CodeModel.Status != MyCommon.STATUS.취소)
                {
                    lv_order.Items[SelectCodeModel.SelectIndex].SubItems[6].Text =
                        SelectCodeModel.CodeModel.Status.ToString();
                    lv_order.Items[SelectCodeModel.SelectIndex].BackColor =
                        MyCommon.GetBackColorFromStatus(SelectCodeModel.CodeModel.Status);
                }
                else
                    lv_order.Items.RemoveAt(SelectCodeModel.SelectIndex);
            }
            SelectCodeModel = null;
        }

        private void 보류ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CodeModelChangeStatusEvent(MyCommon.STATUS.보류);
        }

        private void 진행ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CodeModelChangeStatusEvent(MyCommon.STATUS.진행);
        }

        private void 취소ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CodeModelChangeStatusEvent(MyCommon.STATUS.취소);
            
        }
        
        private void lv_myWork_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(lv_myWork.SelectedItems.Count != 0)
            {
                MyListViewItem myListViewItem = lv_myWork.SelectedItems[0] as MyListViewItem;
                if (SelectCodeModel != null)
                    SelectCodeModel.SelectIndex = lv_myWork.SelectedIndices[0];
                else
                    SelectCodeModel = new SelectModel(myListViewItem.CodeModel, lv_myWork.SelectedIndices[0]);
            }
            else
                SelectCodeModel = null;
        }

        private void rb_progress_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;
            if (cb_deleteView.Checked && radioButton.Text != "전체")
                cb_deleteView.Checked = false;

            MyCommon.STATUS status = GetMainFormMyWorkStatus();
            MyWorkListLoad(status);
            OrderListLoad(status);
        }

        private void cb_deleteView_CheckedChanged(object sender, EventArgs e)
        {
            if ((sender as CheckBox).Checked)
            {
                if (!rb_all.Checked)
                {
                    if (MessageBox.Show("삭제보기는 '전체' 상태에서만 지원합니다.\n" +
                                    "'전체'로 변경하시겠습니까?", "질문", MessageBoxButtons.YesNo) == DialogResult.No)
                        return;
                    else
                        rb_all.Checked = true;
                }
            }
            MyWorkListLoad(MyCommon.STATUS.전체);
        }

        private void 삭제ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(SelectCodeModel.CodeModel.Status == MyCommon.STATUS.취소)
            {
                DBControl.Instance.UpdateUsing(SelectCodeModel.CodeModel);
                if (!cb_deleteView.Checked)
                    lv_myWork.Items.RemoveAt(SelectCodeModel.SelectIndex);
                else
                {
                    (lv_myWork.Items[SelectCodeModel.SelectIndex].SubItems[3].Text,
                        lv_myWork.Items[SelectCodeModel.SelectIndex].BackColor) = MyCommon.GetBackColorFromNonUsing();
                }
            }
            else
            {
                MessageBox.Show("'취소' 상태인 항목만 삭제하실 수 있습니다.");
            }
        }

        private void 수정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectCodeModel.CodeModel is MyWork)
            {
                MyWorkForm myWorkForm = new MyWorkForm(true);
                myWorkForm.SelectMyWork = SelectCodeModel.CodeModel as MyWork;
                myWorkForm.StartPosition = FormStartPosition.CenterScreen;
                myWorkForm.MyWorkUpdateCallBack += MyWorkForm_MyWorkUpdateCallBack;
                myWorkForm.ShowDialog();
            } else if(SelectCodeModel.CodeModel is HospitalOrder)
            {
                OrderForm orderForm = new OrderForm(true);
                orderForm.SelectHospitalOrder = SelectCodeModel.CodeModel as HospitalOrder;
                orderForm.StartPosition = FormStartPosition.CenterScreen;
                orderForm.UpdateCallback += OrderForm_UpdateCallback;
                orderForm.ShowDialog();
            }
        }

        private void OrderForm_UpdateCallback(HospitalOrder hospitalOrder)
        {
            lv_order.Items[SelectCodeModel.SelectIndex] = 
                new MyListViewItem(SelectCodeModel.CodeModel);
        }

        private void MyWorkForm_MyWorkUpdateCallBack(MyWork myWork)
        {
            string[] items = myWork.ToString().Split(',');
            for (int i = 0; i < 4; i++)
            {
                lv_myWork.Items[SelectCodeModel.SelectIndex].SubItems[i].Text = items[i];
            }
            
        }

        private void 환경설정ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowDataSetForm();
        }

        private void lv_order_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lv_order.SelectedItems.Count != 0)
            {
                MyListViewItem myListViewItem = lv_order.SelectedItems[0] as MyListViewItem;
                if (SelectCodeModel != null)
                    SelectCodeModel.SelectIndex = lv_order.SelectedIndices[0];
                else
                    SelectCodeModel = new SelectModel(myListViewItem.CodeModel, lv_order.SelectedIndices[0]);
            }
            else
                SelectCodeModel = null;
        }

        private void 삭제ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("해당 항목을 정말 삭제 하시겠습니까?", "삭제", MessageBoxButtons.YesNo)
                == DialogResult.Yes)
            {
                if (SelectCodeModel.CodeModel is MyWork)
                {
                    DBControl.Instance.Delete(SelectCodeModel.CodeModel);
                    lv_myWork.Items.RemoveAt(SelectCodeModel.SelectIndex);
                }
                else if(SelectCodeModel.CodeModel is HospitalOrder)
                {
                    DBControl.Instance.DeleteHospitalOrder(SelectCodeModel.CodeModel as HospitalOrder);
                    lv_order.Items.RemoveAt(SelectCodeModel.SelectIndex);
                }

            }
        }

        private void 찾기FToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FindFormShow();
        }

        private void FindFormShow()
        {
            bool IsOrder = (tc_main.SelectedIndex == 0) ? true : false;
            FindForm findForm = new FindForm(IsOrder);
            findForm.StartPosition = FormStartPosition.CenterScreen;
            findForm.FindCallBack += FindForm_FindCallBack;
            findForm.ShowDialog();
        }

        private void FindForm_FindCallBack(ArrayList codeModels)
        {
            if(codeModels.Count == 0)
            {
                MessageBox.Show("검색실패!!!");
                return;
            }

            if (codeModels[0] is HospitalOrder)
                lv_order.Items.Clear();
            else if (codeModels[0] is MyWork)
                lv_myWork.Items.Clear();

            foreach(CodeModel codeModel in codeModels)
            {
                if(codeModel is HospitalOrder)
                {
                    HospitalOrder hospitalOrder = codeModel as HospitalOrder;
                    string[] str = hospitalOrder.ToString().Split(',');
                    str[2] = $"{int.Parse(str[2]):n0}";
                    str[3] = $"{int.Parse(str[3]):n0}";
                    MyListViewItem myListViewItem = new MyListViewItem(str);
                    myListViewItem.CodeModel = hospitalOrder;
                    lv_order.Items.Add(myListViewItem);
                }else if(codeModel is MyWork)
                {
                    MyWork myWork = codeModel as MyWork;
                    MyListViewItem myListViewItem = new MyListViewItem(myWork);
                    lv_myWork.Items.Add(myListViewItem);
                }
            }
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            AddItem();
        }

        private void 통계ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowStatsForm();
        }

        private void ShowStatsForm()
        {
            StatsForm statsForm = new StatsForm(dtp_pre.Value, dtp_from.Value);
            statsForm.StartPosition = FormStartPosition.CenterScreen;
            statsForm.Show();
        }
    }
}
