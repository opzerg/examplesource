﻿using OrderProgram;
namespace OrderProgram
{
    partial class MainForm
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btn_add = new System.Windows.Forms.Button();
            this.cb_deleteView = new System.Windows.Forms.CheckBox();
            this.btn_search = new System.Windows.Forms.Button();
            this.btn_year = new System.Windows.Forms.Button();
            this.btn_month = new System.Windows.Forms.Button();
            this.btn_week = new System.Windows.Forms.Button();
            this.dtp_from = new System.Windows.Forms.DateTimePicker();
            this.dtp_pre = new System.Windows.Forms.DateTimePicker();
            this.gb_state = new System.Windows.Forms.GroupBox();
            this.rb_cancel = new System.Windows.Forms.RadioButton();
            this.rb_all = new System.Windows.Forms.RadioButton();
            this.rb_progress = new System.Windows.Forms.RadioButton();
            this.rb_hold = new System.Windows.Forms.RadioButton();
            this.rb_done = new System.Windows.Forms.RadioButton();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.메뉴ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.병원추가ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.요청자ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.통계ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.환경설정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tc_main = new System.Windows.Forms.TabControl();
            this.tp_order = new System.Windows.Forms.TabPage();
            this.lv_order = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tp_myWork = new System.Windows.Forms.TabPage();
            this.lv_myWork = new System.Windows.Forms.ListView();
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.완료ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.완료ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.보류ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.진행ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.취소ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.수정ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.삭제ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.삭제ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.찾기FToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.gb_state.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tc_main.SuspendLayout();
            this.tp_order.SuspendLayout();
            this.tp_myWork.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.Color.Ivory;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.Color.Ivory;
            this.splitContainer1.Panel1.Controls.Add(this.btn_add);
            this.splitContainer1.Panel1.Controls.Add(this.cb_deleteView);
            this.splitContainer1.Panel1.Controls.Add(this.btn_search);
            this.splitContainer1.Panel1.Controls.Add(this.btn_year);
            this.splitContainer1.Panel1.Controls.Add(this.btn_month);
            this.splitContainer1.Panel1.Controls.Add(this.btn_week);
            this.splitContainer1.Panel1.Controls.Add(this.dtp_from);
            this.splitContainer1.Panel1.Controls.Add(this.dtp_pre);
            this.splitContainer1.Panel1.Controls.Add(this.gb_state);
            this.splitContainer1.Panel1.Controls.Add(this.menuStrip1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.Ivory;
            this.splitContainer1.Panel2.Controls.Add(this.tc_main);
            this.splitContainer1.Size = new System.Drawing.Size(1249, 696);
            this.splitContainer1.SplitterDistance = 166;
            this.splitContainer1.TabIndex = 0;
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(1155, 133);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(82, 29);
            this.btn_add.TabIndex = 11;
            this.btn_add.Text = "추가";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // cb_deleteView
            // 
            this.cb_deleteView.AutoSize = true;
            this.cb_deleteView.Enabled = false;
            this.cb_deleteView.Location = new System.Drawing.Point(484, 134);
            this.cb_deleteView.Name = "cb_deleteView";
            this.cb_deleteView.Size = new System.Drawing.Size(197, 41);
            this.cb_deleteView.TabIndex = 1;
            this.cb_deleteView.Text = "삭제보기";
            this.cb_deleteView.UseVisualStyleBackColor = true;
            this.cb_deleteView.Visible = false;
            this.cb_deleteView.CheckedChanged += new System.EventHandler(this.cb_deleteView_CheckedChanged);
            // 
            // btn_search
            // 
            this.btn_search.Location = new System.Drawing.Point(233, 31);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(35, 29);
            this.btn_search.TabIndex = 10;
            this.btn_search.Text = "button1";
            this.btn_search.UseVisualStyleBackColor = true;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // btn_year
            // 
            this.btn_year.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_year.Location = new System.Drawing.Point(65, 62);
            this.btn_year.Name = "btn_year";
            this.btn_year.Size = new System.Drawing.Size(51, 19);
            this.btn_year.TabIndex = 9;
            this.btn_year.Text = "Year";
            this.btn_year.UseVisualStyleBackColor = true;
            this.btn_year.Click += new System.EventHandler(this.btn_year_Click);
            // 
            // btn_month
            // 
            this.btn_month.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_month.Location = new System.Drawing.Point(122, 62);
            this.btn_month.Name = "btn_month";
            this.btn_month.Size = new System.Drawing.Size(53, 19);
            this.btn_month.TabIndex = 8;
            this.btn_month.Text = "Month";
            this.btn_month.UseVisualStyleBackColor = true;
            this.btn_month.Click += new System.EventHandler(this.btn_month_Click);
            // 
            // btn_week
            // 
            this.btn_week.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_week.Location = new System.Drawing.Point(176, 62);
            this.btn_week.Name = "btn_week";
            this.btn_week.Size = new System.Drawing.Size(51, 19);
            this.btn_week.TabIndex = 7;
            this.btn_week.Text = "Week";
            this.btn_week.UseVisualStyleBackColor = true;
            this.btn_week.Click += new System.EventHandler(this.btn_week_Click);
            // 
            // dtp_from
            // 
            this.dtp_from.CustomFormat = "yyyy-MM-dd";
            this.dtp_from.Font = new System.Drawing.Font("굴림", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dtp_from.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_from.Location = new System.Drawing.Point(122, 33);
            this.dtp_from.Name = "dtp_from";
            this.dtp_from.Size = new System.Drawing.Size(105, 39);
            this.dtp_from.TabIndex = 6;
            // 
            // dtp_pre
            // 
            this.dtp_pre.CustomFormat = "yyyy-MM-dd";
            this.dtp_pre.Font = new System.Drawing.Font("굴림", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.dtp_pre.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp_pre.Location = new System.Drawing.Point(15, 33);
            this.dtp_pre.Name = "dtp_pre";
            this.dtp_pre.Size = new System.Drawing.Size(101, 39);
            this.dtp_pre.TabIndex = 5;
            // 
            // gb_state
            // 
            this.gb_state.Controls.Add(this.rb_cancel);
            this.gb_state.Controls.Add(this.rb_all);
            this.gb_state.Controls.Add(this.rb_progress);
            this.gb_state.Controls.Add(this.rb_hold);
            this.gb_state.Controls.Add(this.rb_done);
            this.gb_state.Font = new System.Drawing.Font("굴림", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.gb_state.Location = new System.Drawing.Point(12, 116);
            this.gb_state.Name = "gb_state";
            this.gb_state.Size = new System.Drawing.Size(466, 47);
            this.gb_state.TabIndex = 1;
            this.gb_state.TabStop = false;
            this.gb_state.Text = "상태";
            // 
            // rb_cancel
            // 
            this.rb_cancel.AutoSize = true;
            this.rb_cancel.Location = new System.Drawing.Point(407, 20);
            this.rb_cancel.Name = "rb_cancel";
            this.rb_cancel.Size = new System.Drawing.Size(97, 31);
            this.rb_cancel.TabIndex = 5;
            this.rb_cancel.Text = "취소";
            this.rb_cancel.UseVisualStyleBackColor = true;
            this.rb_cancel.Click += new System.EventHandler(this.rb_progress_CheckedChanged);
            // 
            // rb_all
            // 
            this.rb_all.AutoSize = true;
            this.rb_all.Location = new System.Drawing.Point(312, 20);
            this.rb_all.Name = "rb_all";
            this.rb_all.Size = new System.Drawing.Size(97, 31);
            this.rb_all.TabIndex = 4;
            this.rb_all.Text = "전체";
            this.rb_all.UseVisualStyleBackColor = true;
            this.rb_all.Click += new System.EventHandler(this.rb_progress_CheckedChanged);
            // 
            // rb_progress
            // 
            this.rb_progress.AutoSize = true;
            this.rb_progress.Checked = true;
            this.rb_progress.Location = new System.Drawing.Point(209, 20);
            this.rb_progress.Name = "rb_progress";
            this.rb_progress.Size = new System.Drawing.Size(97, 31);
            this.rb_progress.TabIndex = 3;
            this.rb_progress.TabStop = true;
            this.rb_progress.Text = "진행";
            this.rb_progress.UseVisualStyleBackColor = true;
            this.rb_progress.Click += new System.EventHandler(this.rb_progress_CheckedChanged);
            // 
            // rb_hold
            // 
            this.rb_hold.AutoSize = true;
            this.rb_hold.Location = new System.Drawing.Point(106, 20);
            this.rb_hold.Name = "rb_hold";
            this.rb_hold.Size = new System.Drawing.Size(97, 31);
            this.rb_hold.TabIndex = 2;
            this.rb_hold.Text = "보류";
            this.rb_hold.UseVisualStyleBackColor = true;
            this.rb_hold.Click += new System.EventHandler(this.rb_progress_CheckedChanged);
            // 
            // rb_done
            // 
            this.rb_done.AutoSize = true;
            this.rb_done.Location = new System.Drawing.Point(3, 20);
            this.rb_done.Name = "rb_done";
            this.rb_done.Size = new System.Drawing.Size(97, 31);
            this.rb_done.TabIndex = 0;
            this.rb_done.Text = "완료";
            this.rb_done.UseVisualStyleBackColor = true;
            this.rb_done.Click += new System.EventHandler(this.rb_progress_CheckedChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.Ivory;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.메뉴ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1249, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 메뉴ToolStripMenuItem
            // 
            this.메뉴ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.병원추가ToolStripMenuItem,
            this.요청자ToolStripMenuItem,
            this.통계ToolStripMenuItem,
            this.환경설정ToolStripMenuItem});
            this.메뉴ToolStripMenuItem.Name = "메뉴ToolStripMenuItem";
            this.메뉴ToolStripMenuItem.Size = new System.Drawing.Size(75, 36);
            this.메뉴ToolStripMenuItem.Text = "메뉴";
            // 
            // 병원추가ToolStripMenuItem
            // 
            this.병원추가ToolStripMenuItem.Name = "병원추가ToolStripMenuItem";
            this.병원추가ToolStripMenuItem.Size = new System.Drawing.Size(265, 38);
            this.병원추가ToolStripMenuItem.Text = "병원(H)";
            this.병원추가ToolStripMenuItem.Click += new System.EventHandler(this.병원추가ToolStripMenuItem_Click);
            // 
            // 요청자ToolStripMenuItem
            // 
            this.요청자ToolStripMenuItem.Name = "요청자ToolStripMenuItem";
            this.요청자ToolStripMenuItem.Size = new System.Drawing.Size(265, 38);
            this.요청자ToolStripMenuItem.Text = "파트너(P)";
            this.요청자ToolStripMenuItem.Click += new System.EventHandler(this.요청자ToolStripMenuItem_Click);
            // 
            // 통계ToolStripMenuItem
            // 
            this.통계ToolStripMenuItem.Name = "통계ToolStripMenuItem";
            this.통계ToolStripMenuItem.Size = new System.Drawing.Size(265, 38);
            this.통계ToolStripMenuItem.Text = "통계(S)";
            this.통계ToolStripMenuItem.Click += new System.EventHandler(this.통계ToolStripMenuItem_Click);
            // 
            // 환경설정ToolStripMenuItem
            // 
            this.환경설정ToolStripMenuItem.Name = "환경설정ToolStripMenuItem";
            this.환경설정ToolStripMenuItem.Size = new System.Drawing.Size(265, 38);
            this.환경설정ToolStripMenuItem.Text = "데이터설정(D)";
            this.환경설정ToolStripMenuItem.Click += new System.EventHandler(this.환경설정ToolStripMenuItem_Click);
            // 
            // tc_main
            // 
            this.tc_main.Controls.Add(this.tp_order);
            this.tc_main.Controls.Add(this.tp_myWork);
            this.tc_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tc_main.Location = new System.Drawing.Point(0, 0);
            this.tc_main.Name = "tc_main";
            this.tc_main.SelectedIndex = 0;
            this.tc_main.Size = new System.Drawing.Size(1249, 526);
            this.tc_main.TabIndex = 1;
            // 
            // tp_order
            // 
            this.tp_order.BackColor = System.Drawing.SystemColors.Window;
            this.tp_order.Controls.Add(this.lv_order);
            this.tp_order.Location = new System.Drawing.Point(8, 52);
            this.tp_order.Name = "tp_order";
            this.tp_order.Padding = new System.Windows.Forms.Padding(3);
            this.tp_order.Size = new System.Drawing.Size(1233, 466);
            this.tp_order.TabIndex = 0;
            this.tp_order.Text = "발주현황";
            // 
            // lv_order
            // 
            this.lv_order.BackColor = System.Drawing.Color.White;
            this.lv_order.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader13,
            this.columnHeader14});
            this.lv_order.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv_order.FullRowSelect = true;
            this.lv_order.Location = new System.Drawing.Point(3, 3);
            this.lv_order.Name = "lv_order";
            this.lv_order.Size = new System.Drawing.Size(1227, 460);
            this.lv_order.TabIndex = 0;
            this.lv_order.UseCompatibleStateImageBehavior = false;
            this.lv_order.View = System.Windows.Forms.View.Details;
            this.lv_order.SelectedIndexChanged += new System.EventHandler(this.lv_order_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "날짜";
            this.columnHeader1.Width = 162;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "병원명";
            this.columnHeader2.Width = 282;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "합계금액(원)";
            this.columnHeader3.Width = 174;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "합계부가세(원)";
            this.columnHeader4.Width = 214;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "요청자";
            this.columnHeader5.Width = 128;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "요청건수";
            this.columnHeader13.Width = 174;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "상태";
            this.columnHeader14.Width = 86;
            // 
            // tp_myWork
            // 
            this.tp_myWork.Controls.Add(this.lv_myWork);
            this.tp_myWork.Location = new System.Drawing.Point(8, 52);
            this.tp_myWork.Name = "tp_myWork";
            this.tp_myWork.Padding = new System.Windows.Forms.Padding(3);
            this.tp_myWork.Size = new System.Drawing.Size(1233, 466);
            this.tp_myWork.TabIndex = 1;
            this.tp_myWork.Text = "내 업무";
            this.tp_myWork.UseVisualStyleBackColor = true;
            // 
            // lv_myWork
            // 
            this.lv_myWork.BackColor = System.Drawing.Color.White;
            this.lv_myWork.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columnHeader9});
            this.lv_myWork.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv_myWork.FullRowSelect = true;
            this.lv_myWork.Location = new System.Drawing.Point(3, 3);
            this.lv_myWork.Name = "lv_myWork";
            this.lv_myWork.Size = new System.Drawing.Size(1227, 460);
            this.lv_myWork.TabIndex = 0;
            this.lv_myWork.UseCompatibleStateImageBehavior = false;
            this.lv_myWork.View = System.Windows.Forms.View.Details;
            this.lv_myWork.SelectedIndexChanged += new System.EventHandler(this.lv_myWork_SelectedIndexChanged);
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "날짜";
            this.columnHeader6.Width = 202;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "요청자";
            this.columnHeader7.Width = 174;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "요청수";
            this.columnHeader8.Width = 140;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "상태";
            this.columnHeader9.Width = 184;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.완료ToolStripMenuItem,
            this.수정ToolStripMenuItem,
            this.삭제ToolStripMenuItem,
            this.삭제ToolStripMenuItem1,
            this.찾기FToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(165, 184);
            // 
            // 완료ToolStripMenuItem
            // 
            this.완료ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.완료ToolStripMenuItem1,
            this.보류ToolStripMenuItem,
            this.진행ToolStripMenuItem,
            this.취소ToolStripMenuItem});
            this.완료ToolStripMenuItem.Name = "완료ToolStripMenuItem";
            this.완료ToolStripMenuItem.Size = new System.Drawing.Size(164, 36);
            this.완료ToolStripMenuItem.Text = "상태";
            // 
            // 완료ToolStripMenuItem1
            // 
            this.완료ToolStripMenuItem1.Name = "완료ToolStripMenuItem1";
            this.완료ToolStripMenuItem1.Size = new System.Drawing.Size(162, 38);
            this.완료ToolStripMenuItem1.Text = "완료";
            this.완료ToolStripMenuItem1.Click += new System.EventHandler(this.완료ToolStripMenuItem1_Click);
            // 
            // 보류ToolStripMenuItem
            // 
            this.보류ToolStripMenuItem.Name = "보류ToolStripMenuItem";
            this.보류ToolStripMenuItem.Size = new System.Drawing.Size(162, 38);
            this.보류ToolStripMenuItem.Text = "보류";
            this.보류ToolStripMenuItem.Click += new System.EventHandler(this.보류ToolStripMenuItem_Click);
            // 
            // 진행ToolStripMenuItem
            // 
            this.진행ToolStripMenuItem.Name = "진행ToolStripMenuItem";
            this.진행ToolStripMenuItem.Size = new System.Drawing.Size(162, 38);
            this.진행ToolStripMenuItem.Text = "진행";
            this.진행ToolStripMenuItem.Click += new System.EventHandler(this.진행ToolStripMenuItem_Click);
            // 
            // 취소ToolStripMenuItem
            // 
            this.취소ToolStripMenuItem.Name = "취소ToolStripMenuItem";
            this.취소ToolStripMenuItem.Size = new System.Drawing.Size(162, 38);
            this.취소ToolStripMenuItem.Text = "취소";
            this.취소ToolStripMenuItem.Click += new System.EventHandler(this.취소ToolStripMenuItem_Click);
            // 
            // 수정ToolStripMenuItem
            // 
            this.수정ToolStripMenuItem.Name = "수정ToolStripMenuItem";
            this.수정ToolStripMenuItem.Size = new System.Drawing.Size(164, 36);
            this.수정ToolStripMenuItem.Text = "수정";
            this.수정ToolStripMenuItem.Click += new System.EventHandler(this.수정ToolStripMenuItem_Click);
            // 
            // 삭제ToolStripMenuItem
            // 
            this.삭제ToolStripMenuItem.Enabled = false;
            this.삭제ToolStripMenuItem.Name = "삭제ToolStripMenuItem";
            this.삭제ToolStripMenuItem.Size = new System.Drawing.Size(164, 36);
            this.삭제ToolStripMenuItem.Text = "삭제";
            this.삭제ToolStripMenuItem.Visible = false;
            this.삭제ToolStripMenuItem.Click += new System.EventHandler(this.삭제ToolStripMenuItem_Click);
            // 
            // 삭제ToolStripMenuItem1
            // 
            this.삭제ToolStripMenuItem1.Name = "삭제ToolStripMenuItem1";
            this.삭제ToolStripMenuItem1.Size = new System.Drawing.Size(164, 36);
            this.삭제ToolStripMenuItem1.Text = "삭제";
            this.삭제ToolStripMenuItem1.Click += new System.EventHandler(this.삭제ToolStripMenuItem1_Click);
            // 
            // 찾기FToolStripMenuItem
            // 
            this.찾기FToolStripMenuItem.Name = "찾기FToolStripMenuItem";
            this.찾기FToolStripMenuItem.Size = new System.Drawing.Size(164, 36);
            this.찾기FToolStripMenuItem.Text = "찾기(F)";
            this.찾기FToolStripMenuItem.Click += new System.EventHandler(this.찾기FToolStripMenuItem_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(22F, 37F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1249, 696);
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("굴림", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(5);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "메인화면";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.gb_state.ResumeLayout(false);
            this.gb_state.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tc_main.ResumeLayout(false);
            this.tp_order.ResumeLayout(false);
            this.tp_myWork.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 메뉴ToolStripMenuItem;
        private System.Windows.Forms.TabControl tc_main;
        private System.Windows.Forms.TabPage tp_order;
        private System.Windows.Forms.TabPage tp_myWork;
        private System.Windows.Forms.ListView lv_order;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ListView lv_myWork;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ToolStripMenuItem 병원추가ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 요청자ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 통계ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 환경설정ToolStripMenuItem;
        private System.Windows.Forms.GroupBox gb_state;
        private System.Windows.Forms.RadioButton rb_progress;
        private System.Windows.Forms.RadioButton rb_hold;
        private System.Windows.Forms.RadioButton rb_done;
        private System.Windows.Forms.RadioButton rb_all;
        private System.Windows.Forms.DateTimePicker dtp_pre;
        private System.Windows.Forms.DateTimePicker dtp_from;
        private System.Windows.Forms.Button btn_month;
        private System.Windows.Forms.Button btn_week;
        private System.Windows.Forms.Button btn_year;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 완료ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 완료ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 보류ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 진행ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 수정ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 삭제ToolStripMenuItem;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.ToolStripMenuItem 취소ToolStripMenuItem;
        private System.Windows.Forms.RadioButton rb_cancel;
        private System.Windows.Forms.CheckBox cb_deleteView;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ToolStripMenuItem 삭제ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 찾기FToolStripMenuItem;
        private System.Windows.Forms.Button btn_add;
    }
}

