﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OrderProgram.Model
{
    public class MyListViewItem : ListViewItem
    {
        public CodeModel CodeModel
        {
            get;
            set;
        }
        public MyListViewItem
            (CodeModel codeModel) : 
            base(codeModel.ToString().Split(','))
        {
            this.CodeModel = codeModel;
        }

        public MyListViewItem
            (string[] items) :
            base(items)
        {

        }
    }
}
