﻿using OrderProgram.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderProgram.Model
{
    public class HospitalOrder : CodeModel
    {
        
        public HospitalOrder()
        {
            this.Sequence = 0;
        }

        public HospitalOrder(string request_date, 
            Hospital hospital, Partner partner, MyCommon.STATUS status = MyCommon.STATUS.진행)
        {
            this.NAME = partner.NAME;
            this.RequestDate = request_date;
            this.Hospital = hospital;
            this.Partner = partner;
            this.Status = status;
            Orders = new List<Order>();
        }

        public HospitalOrder(int sequence, string request_date, 
            Hospital hospital, Partner partner, MyCommon.STATUS status)
        {
            this.NAME = partner.NAME;
            this.CODE = sequence.ToString();
            this.Sequence = sequence;
            this.RequestDate = request_date;
            this.Hospital = hospital;
            this.Partner = partner;
            this.Status = status;
            Orders = new List<Order>();
        }

        public int Sequence
        {
            get; set;
        }

        public Hospital Hospital
        {
            get; set;
        }

        public Partner Partner
        {
            get; set;
        }

        public List<Order> Orders
        {
            get; set;
        }

        public override string ToString()
        {
            long totalPriace = 0;
            long totalSurtax = 0;
            foreach (Order order in Orders)
            {
                totalPriace += order.Price;
                totalSurtax += order.Surtax;
            }
            
            return $"{RequestDate},{Hospital.NAME},{totalPriace},{totalSurtax},{Partner.NAME}," +
                $"{Orders.Count()},{Status}";
        }


        public class Order : CodeModel
        {
            public Order()
            {
                this.Sequence = 0;
            }

            public int HospitalSequnce
            {
                get; set;
            }

            public Order(int sequence, int hospitalSequence, MyCommon.ORDER_KIND kind, 
                Category large, Category small, int box, long price, long surtax)
            {
                this.Sequence = sequence;
                this.HospitalSequnce = hospitalSequence;
                this.OrderKind = kind;
                this.LargeCategory = large;
                this.SmallCategory = small;
                this.Box = box;
                this.Price = price;
                this.Surtax = surtax;
            }

            public int Sequence
            {
                get;
            }

            public MyCommon.ORDER_KIND OrderKind
            {
                get; set;
            }

            public Category LargeCategory
            {
                get; set;
            }
            public Category SmallCategory
            {
                get; set;
            }
            public int Box
            {
                get; set;
            }
            public long Price
            {
                get; set;
            }
            public long Surtax
            {
                get; set;
            }
            public override string ToString()
            {
                return $"{this.Sequence},{this.OrderKind},{this.LargeCategory}," +
                    $"{this.SmallCategory},{this.Box},{this.Price},{this.Surtax}";
            }

            internal void Change(Order nowOrder)
            {
                OrderKind = nowOrder.OrderKind;
                SmallCategory = nowOrder.SmallCategory;
                LargeCategory = nowOrder.LargeCategory;
                Box = nowOrder.Box;
                Price = nowOrder.Price;
                Surtax = nowOrder.Surtax;
            }
        }
        
        
    }
}
