﻿using OrderProgram.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderProgram.Model
{
    public class CodeModel
    {
        public string RequestDate
        {
            get; set;
        }
        public string CODE
        {
            get; set;
        }

        public string NAME
        {
            get; set;
        }
        public MyCommon.FLAG? FLAG
        {
            get; set;
        }
        public MyCommon.STATUS Status
        {
            get; set;
        }
        public CodeModel()
        {
            CODE = string.Empty;
        }

        public CodeModel(string code)
        {
            CODE = code;
        }

        public CodeModel(string code, string name)
        {
            this.CODE = code;
            this.NAME = name;
        }

        public CodeModel(string code, string name, MyCommon.FLAG? flag = MyCommon.FLAG.USING)
        {
            this.CODE = code;
            this.NAME = name;
            this.FLAG = flag;
            this.RequestDate = string.Empty;
            this.Status = MyCommon.STATUS.NONE;
        }

        public override string ToString()
        {
            return $"{CODE},{NAME}";
        }
    }
}
