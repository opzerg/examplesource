﻿using OrderProgram.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderProgram.Model
{
    public class Category : CodeModel
    {
        public MyCommon.CATEGORY_KIND KIND
        {
            get; set;
        }
        public Category(string code, string name, MyCommon.CATEGORY_KIND kind) : base(code, name)
        {
            this.KIND = kind;
        }

        public Category(MyCommon.CATEGORY_KIND kind)
        {
            KIND = kind;
        }

        public Category(string code, MyCommon.CATEGORY_KIND kind)
        {
            this.CODE = code;
            KIND = kind;
        }

        public override string ToString()
        {
            return $"{this.NAME}";
        }
    }
}
