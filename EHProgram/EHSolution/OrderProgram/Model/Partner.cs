﻿using OrderProgram.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderProgram.Model
{
    public class Partner : CodeModel
    {
        public string PHONE
        {
            get;set;
        }
        public string MEMO
        {
            get;set;
        }
        
        public void Change(Partner partner)
        {
            this.NAME = partner.NAME;
            this.PHONE = partner.PHONE;
            this.MEMO = partner.MEMO;
            this.FLAG = partner.FLAG;
        }

        public Partner() : base()
        {
        }

        public Partner(string code): base(code)
        {

        }


        public Partner(string code, string name, string phone, 
            string memo, MyCommon.FLAG? flag = MyCommon.FLAG.USING)
            :base(code, name)
        {
            this.PHONE = phone;
            this.MEMO = memo;
            this.FLAG = flag;
        }
        public override string ToString()
        {
            return $"{this.CODE},{this.NAME}";
        }
    }
}
