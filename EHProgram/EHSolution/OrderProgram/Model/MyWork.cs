﻿using OrderProgram.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderProgram.Model
{
    public class MyWork : CodeModel
    {
        public Partner Partner
        {
            get; set;
        }
        public List<string> Works
        {
            get; set;
        }
      
        public MyWork() 
        {
            RequestDate = null;
        }

        public void Change(MyWork myWork)
        {
            this.RequestDate = myWork.RequestDate;
            this.Partner = myWork.Partner;
            this.Works = myWork.Works;
            this.Status = myWork.Status;
        }
        
        public MyWork(string requestDate, Partner partner, List<string> works, 
            MyCommon.STATUS status = MyCommon.STATUS.진행, MyCommon.FLAG flag = MyCommon.FLAG.USING) : base(partner.CODE)
        {
            this.NAME = partner.NAME;
            this.RequestDate = requestDate;
            this.Partner = partner;
            this.Works = works;
            this.Status = status;
            this.FLAG = flag;
        }

        public override string ToString()
        {
            return $"{RequestDate},{Partner.NAME},{Works.Count},{Status.ToString()}";
        }
    }
}
