﻿using OrderProgram.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderProgram.Model
{
    public class Hospital : CodeModel
    {
        public string MANAGER
        {
            get;set;
        }
        public string PHONE
        {
            get;set;
        }
        public string ADDRESS
        {
            get; set;
        }
        public string MEMO
        {
            get; set;
        }
        

        public Hospital():base()
        {
        }

        public Hospital(string code):base(code)
        {
        }

        public void Change(Hospital hospital)
        {
            this.NAME = hospital.NAME;
            this.MANAGER = hospital.MANAGER;
            this.PHONE = hospital.PHONE;
            this.ADDRESS = hospital.ADDRESS;
            this.MEMO = hospital.MEMO;
            this.FLAG = hospital.FLAG;
        }

        public Hospital(string code, string name, string manager, 
            string phone, string address, string memo, MyCommon.FLAG? flag = MyCommon.FLAG.USING)
            : base(code,name)
        {
            MANAGER = manager;
            PHONE = phone;
            ADDRESS = address;
            MEMO = memo;
            FLAG = flag;
        }


    }
}
